#!/bin/bash
set -e
echo "### Cron Setup ###"
# install & setup cron
apt-get update && apt-get -y install cron
CRONTAB_PATH="/etc/cron.d/owqc-cron"
touch $CRONTAB_PATH
chmod 0644 $CRONTAB_PATH
crontab $CRONTAB_PATH

# save environment variables for owqc
env > /app/.env

# define the job and make it executable
JOB_PATH=/app/cron_job.sh
chmod +x $JOB_PATH

# add job
if [ -z "$CRON_EXPR" ]; then
  # every friday at 20:00 UTC = 22:00h 
  CRON_EXPR="0 20 * * 5"
  export CRON_EXPR
fi
echo "CRON_EXPR is set to $CRON_EXPR"
SHELL="SHELL=/bin/bash"
PATH="PATH=/app/.venv/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
CRON_LOG=/var/owqc_cron.log
touch $CRON_LOG
(crontab -l ; echo -e "$SHELL\n$PATH\n$CRON_EXPR echo \$(date) owqc >> $CRON_LOG;$JOB_PATH") | crontab -

# echo crontab details
echo "--- new Crontab content ---"
crontab -l
echo "--- end new Crontab content ---"

# startup cron
cron

# dont' terminate container
echo "### Endless loop ###"
tail -f $CRON_LOG
