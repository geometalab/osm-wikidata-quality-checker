# https://binx.io/2022/06/13/poetry-docker/

FROM python:3.10 as python
ENV PYTHONUNBUFFERED=true

RUN apt-get update && apt-get install -y --no-install-recommends gcc libffi-dev g++

WORKDIR /app

FROM python as poetry
ENV POETRY_HOME=/opt/poetry
ENV POETRY_VIRTUALENVS_IN_PROJECT=true
ENV PATH="$POETRY_HOME/bin:$PATH"
RUN python -c 'from urllib.request import urlopen; print(urlopen("https://install.python-poetry.org").read().decode())' | python -
COPY . ./
RUN poetry install --no-interaction --no-ansi -vvv

FROM python as runtime
ENV PATH="/app/.venv/bin:$PATH"
COPY --from=poetry /app /app

VOLUME [ "/data/dumps", "/data/db", "/data/reports", "/data/logs" ]
ENV DUMP_FOLDER="/data/dumps" \
  DB_FOLDER="/data/db" \
  REPORT_FOLDER="/data/reports" \
  LOG_FOLDER="/data/logs"


CMD exec python -m osm_wikidata_quality_checker