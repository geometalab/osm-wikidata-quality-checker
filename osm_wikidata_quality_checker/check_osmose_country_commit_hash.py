import logging
import os

import requests

from osm_wikidata_quality_checker import env

path_2_last_commit_hash = os.path.join(
    env.PATH_2_OSMOSE_COUNTRY_CONFIG, "osmose_country_config_commit_hash.txt"
)


def check_commit_hash(last_commit_has_path: str = path_2_last_commit_hash) -> bool:
    last_commit_hash = _load_last_commit_hash(last_commit_has_path)
    actual_commit_hash = _get_actual_commit_hash()
    if actual_commit_hash == last_commit_hash:
        logging.info("osmose country config last commit hash ok")
        return True
    logging.warning(
        f"osmose country config last commit hash does not match: is '{actual_commit_hash}' was '{last_commit_hash}'"
    )
    return False


def _get_actual_commit_hash() -> str:
    response = requests.get(
        "https://api.github.com/repos/osm-fr/osmose-backend/commits?path=osmose_config.py&ref=main"
    )
    return response.json()[0]["sha"]


def _load_last_commit_hash(path: str = path_2_last_commit_hash) -> str:
    with open(path, "r") as f:
        return f.read().strip()
