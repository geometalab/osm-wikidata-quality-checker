import bz2
import json
from typing import Any, Callable, List

from .import_common import import_controller
from .wd_constants import (
    P_COORDINATE_LOCATION,
    P_COUNTRY,
    P_DISSOLVED,
    P_INSTANCE_OF,
    P_LOCATED_ON_STREET,
    P_LOS_STREET_NR,
    P_POSTAL_CODE,
    P_REPLACED_BY,
    P_STREET_ADDRESS,
    P_SUBCLASS_OF,
)
from .wd_database import WdCoordinate, WdDatabase, WdItem

WD_IMPORT_BULK_SIZE = 500


def _get_numeric_ids_of_claim(entity, code: str) -> List[int]:
    return [
        i.get("mainsnak", {})
        .get("datavalue", {})
        .get("value", {})
        .get("numeric-id", None)
        for i in entity["claims"].get(code, [])
    ]


def _get_fist_numeric_id_of_claim(entity, code: str) -> int | None:
    numeric_ids = _get_numeric_ids_of_claim(entity, code)
    if len(numeric_ids) == 0:
        return None
    if len(numeric_ids) > 1:
        return -1
    return numeric_ids[0]


def _get_claim_values(entity, code: str) -> List[Any]:
    return [
        i.get("mainsnak", {}).get("datavalue", {}).get("value", {})
        for i in entity["claims"].get(code, [])
    ]


def _get_coordinates(entity):
    vals = [
        c.get("mainsnak", {}).get("datavalue", {}).get("value", {})
        for c in entity["claims"].get(P_COORDINATE_LOCATION, [])
    ]

    coords = []
    for val in vals:
        if "latitude" in val and "longitude" in val:
            wd_coord = WdCoordinate(val["latitude"], val["longitude"])
            coords.append(wd_coord)

    return coords


def _create_item(entity) -> WdItem:
    # pylint: disable=protected-access
    item = WdItem()
    item.qid = int(entity["id"][1:])

    item.names = {}
    labels = entity.get("labels", {})
    for lang in labels:
        name = labels.get(lang, {}).get("value", None)
        if name:
            item.names[lang] = name

    item.instances_of = set(_get_numeric_ids_of_claim(entity, P_INSTANCE_OF))
    item.subclasses_of = set(_get_numeric_ids_of_claim(entity, P_SUBCLASS_OF))

    item.country_qid = _get_fist_numeric_id_of_claim(entity, P_COUNTRY)
    _replaced_by_qid = _get_fist_numeric_id_of_claim(entity, P_REPLACED_BY)
    item.replaced_by_qid = (
        _replaced_by_qid if _replaced_by_qid and _replaced_by_qid > 0 else None
    )
    item.street_addresses = _get_claim_values(entity, P_STREET_ADDRESS)
    item.postal_codes = _get_claim_values(entity, P_POSTAL_CODE)

    if dissolved_values := _get_claim_values(entity, P_DISSOLVED):
        item.dissolved = dissolved_values[0].get("time", None)

    item.located_on_streets = []
    for los in entity.get("claims", {}).get(P_LOCATED_ON_STREET, []):
        ref_to_qid = (
            los.get("mainsnak", {})
            .get("datavalue", {})
            .get("value", {})
            .get("numeric-id", 0)
        )
        street_nrs = [
            snr.get("datavalue", {}).get("value", "")
            for snr in los.get("qualifiers", {}).get(P_LOS_STREET_NR, [])
        ]
        item.located_on_streets.append((ref_to_qid, street_nrs))

    item.coordinates = _get_coordinates(entity)

    return item


def _import_function(path_2_wd_dump: str, insert_function: Callable):
    with bz2.open(path_2_wd_dump) as file:
        for line in file:
            # Skip leading '[' and trailing ']' in file.
            if len(line) <= 2:
                continue
            # Strip ',' character at end of line (line[-1] is newline)
            if line[-2] == 44:
                line = line[:-2]

            wd_item = json.loads(line)

            if wd_item["id"][:1] != "Q":
                continue

            item = _create_item(wd_item)

            insert_function(item)


def wd_import(
    path_2_db: str,
    path_2_dump: str,
    bulk_size: int = WD_IMPORT_BULK_SIZE,
    use_multiprocessing: bool = True,
):
    import_controller(
        path_2_db,
        path_2_dump,
        WdDatabase,
        _import_function,
        bulk_size,
        use_multiprocessing,
        process_name="wd_import",
    )
