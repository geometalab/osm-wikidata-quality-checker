import logging
from multiprocessing import JoinableQueue, Process
from typing import Callable, Generic, List, Type, TypeVar

from .database_common import AbstractDatabase

DEFAULT_BULK_SIZE = 1000
DEFAULT_QUEUE_MAX = 30

T = TypeVar("T")  # pylint: disable=invalid-name


class InsertBuffer(Generic[T]):
    def __init__(
        self,
        insert_function: Callable,
        bulk_size: int = DEFAULT_BULK_SIZE,
    ):
        self.insert_function = insert_function
        self.bulk: List[T] = []
        self.bulk_size = bulk_size

    def flush(self):
        self.insert_function(self.bulk)
        self.bulk = []

    def insert(self, osm_obj: T):
        self.bulk.append(osm_obj)
        if len(self.bulk) >= self.bulk_size:
            self.flush()


def queue_to_database(
    queue: JoinableQueue, db_class: Type[AbstractDatabase], path_2_db: str
):
    database = db_class(path_2_db)
    try:
        while True:
            database.add_all(queue.get())
            queue.task_done()
    except Exception as e:
        logging.error("Exception in queue_to_database: %s", str(e))
        raise


def import_controller(
    path_2_db: str,
    path_2_dump: str,
    db_class: Type[AbstractDatabase],
    import_function: Callable,
    bulk_size: int = DEFAULT_BULK_SIZE,
    use_multiprocessing: bool = True,
    process_name="queue_to_database",
):
    if use_multiprocessing:
        queue: JoinableQueue = JoinableQueue(maxsize=DEFAULT_QUEUE_MAX)
        mp_buffer: InsertBuffer = InsertBuffer(queue.put, bulk_size)
        db_process = Process(
            name=process_name,
            target=queue_to_database,
            args=(queue, db_class, path_2_db),
            daemon=True,
        )
        db_process.start()
        import_function(path_2_dump, mp_buffer.insert)
        mp_buffer.flush()
        queue.join()
        db_process.terminate()
        db_process.join()
        if (code := db_process.exitcode) != -15:  # SIGTERM due to terminate()
            raise Exception(f"rocess '{db_process.name}' failed with exitcode {code}")
    else:
        database = db_class(path_2_db)
        buffer: InsertBuffer = InsertBuffer(database.add_all, bulk_size)
        import_function(path_2_dump, buffer.insert)
        buffer.flush()
