from typing import Callable

from .import_common import import_controller
from .wd_redirects_database import WdRedirect, WdRedirectsDatabase

REDIRECTS_BULK_SIZE = 500


def _import_function(path_2_wd_redirects: str, insert_function: Callable):
    DELIMITER = ","

    with open(path_2_wd_redirects, mode="rt", encoding="UTF-8") as file:
        file.readline()  # skip header
        for line in file:

            # ignore 'L' and others
            if not line.startswith("Q"):
                continue

            from_, to, *_ = line.split(DELIMITER)
            redirect = WdRedirect(int(from_[1:]), int(to[1:]))
            insert_function(redirect)


def redirects_import(
    path_2_db: str,
    path_2_dump: str,
    bulk_size: int = REDIRECTS_BULK_SIZE,
    use_multiprocessing: bool = True,
):
    import_controller(
        path_2_db,
        path_2_dump,
        WdRedirectsDatabase,
        _import_function,
        bulk_size,
        use_multiprocessing,
        process_name="redirects_import",
    )
