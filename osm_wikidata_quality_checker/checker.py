import re
from multiprocessing import JoinableQueue
from urllib.error import HTTPError

from wikidata.client import Client
from wikidata.entity import Entity, EntityId, EntityState

from .checker_issues import (
    ChangeFix,
    CheckerIssue,
    DeleteFix,
    InvalidWdItemFormatIssue,
    Link,
    NotExistingWdItemIssue,
    RedirectedWdItemIssue,
    create_link,
)
from .link_checks import (  # check_primary_tag_instance_of,; check_secondary_tags_instance_of,; check_very_large_distance,; check_living_organism,
    check_places,
    check_primary_tag_instance_of,
    check_secondary_tags_instance_of,
    check_unpermitted_instance,
    check_very_large_distance,
)
from .osm_database import OsmObject
from .wd_database import WdDatabase
from .wd_redirects_database import WdRedirectsDatabase


def _is_valid_format(qid_value: str, key: str) -> bool | None:
    """checking the format and returns True/False or None if the link can be ingored"""
    # Ignore lexems for ethymologies (https://www.wikidata.org/wiki/Wikidata:Lexicographical_data/Documentation#Data_Model)
    if key.endswith("etymology:wikidata") and re.match(
        "^L[0-9]+(-[FS][0-9]*)?$", qid_value
    ):
        return None

    # Ignore keys ending with ':missing' and ':fixme'
    if key.endswith(":missing") or key.endswith(":fixme"):
        return None

    # Check format
    if not qid_value.startswith("Q") or not qid_value[1:].isnumeric():
        return False
    return True


def _is_redirected(
    wd_redirects_db: WdRedirectsDatabase,
    qid: int,
) -> int | None:
    search_qid = qid
    handled_qids = {search_qid}
    while search_qid:
        redirect = wd_redirects_db.get(search_qid)
        if not redirect:
            return search_qid if search_qid != qid else None
        search_qid = redirect.qid_to
        if search_qid in handled_qids:
            # loop in redirects, return None, not able to provide a fix idea
            return None
        handled_qids.add(search_qid)

    return None


def _does_wd_item_exist(qid_str: str) -> bool:
    item: Entity | None
    try:
        item = Client().get(EntityId(qid_str), load=True)
    except HTTPError:
        return False
    if item is None:
        return False
    return item.state is not EntityState.non_existent


def _check_link(link: Link) -> CheckerIssue | None:
    link_checks = (
        # check_living_organism, # see issue 44
        check_unpermitted_instance,
        check_primary_tag_instance_of,
        check_secondary_tags_instance_of,
        check_places,
        check_very_large_distance,
    )
    issue: CheckerIssue | None
    for link_check in link_checks:
        if issue := link_check(link):
            return issue
    return None


def check(
    osm: OsmObject, wd_db: WdDatabase, wd_redirects_db: WdRedirectsDatabase
) -> CheckerIssue | None:
    key: str
    value: str
    for key, value in ((k, v) for k, v in osm.tags.items() if "wikidata" in k):
        for qid_str in value.split(";"):
            # basic checks
            if not (is_valid := _is_valid_format(qid_str, key)):
                if is_valid is None:
                    continue  # ignore this value
                return InvalidWdItemFormatIssue(osm, key, qid_str, fix=DeleteFix(key))
            qid = int(qid_str[1:])
            wd_item = wd_db.get(qid)
            if not wd_item:
                if new_qid := _is_redirected(wd_redirects_db, qid):
                    return RedirectedWdItemIssue(
                        osm,
                        key,
                        qid,
                        "Q" + str(new_qid),
                        fix=ChangeFix(key, "Q" + str(new_qid)),
                    )
                if not _does_wd_item_exist(qid_str):
                    return NotExistingWdItemIssue(osm, key, qid, fix=DeleteFix(key))
                continue  # does exist but not in index, ignore
            # wd-item checks
            return _check_link(create_link(osm, wd_item, key))
    return None


def mp_checker(
    path_2_wd_database: str,
    path_2_wd_redirects_database: str,
    osm_obj_queue: JoinableQueue,
    issue_queue: JoinableQueue,
):
    wd_db = WdDatabase(path_2_wd_database)
    wd_redirects_db = WdRedirectsDatabase(path_2_wd_redirects_database)
    while True:
        osm_objects = osm_obj_queue.get()
        issues = []
        for osm_obj in osm_objects:
            if issue := check(osm_obj, wd_db, wd_redirects_db):
                issues.append(issue)

        if len(issues) > 0:
            issue_queue.put(issues)
            issues = []

        osm_obj_queue.task_done()
