from typing import Set

from . import checks_common as c
from .checker_issues import (
    AreaLink,
    ChangeFix,
    DeleteFix,
    DissolvedWdItemIssue,
    Link,
    LivingOrganismIssue,
    MultiFix,
    NewFix,
    NodeLink,
    PlaceMismatchIssue,
    PrimaryTagClaimMismatchIssue,
    ReplacedWdItemIssue,
    SecondaryTagClaimMismatchIssue,
    UnpermittedInstanceIssue,
    VeryLargeDistanceIssue,
)
from .osm_database import OsmWay
from .wd_constants import (
    PRIMARY_TAG_MATCHES,
    Q_HUMAN,
    Q_INDIVIDUAL_ORGANISM,
    Q_LIST,
    Q_LIVING_ORGANISM,
    Q_MEMORIAL,
    Q_NATURAL_MONUMENT,
    Q_ORGANISMS_KNOWN_BY_A_PARTICULAR_COMMON_NAME,
    Q_TAXON,
    Q_TREE,
    Q_WIKIMEDIA_CATEGORY,
    Q_WIKIMEDIA_DISAMBIGUATION_PAGE,
    Q_WIKIMEDIA_LIST_ARTICLE,
    SECONDARY_WIKIDATA_TAGS,
)


def check_living_organism(link: Link) -> LivingOrganismIssue | None:

    # look for living things in primary tags and certain secondary tags
    if Q_LIVING_ORGANISM not in link.wd.all_instances_and_subclasses or (
        link.key
        not in (
            "wikidata",
            # "brand:wikidata",  # deactivated, see issue#9
            "operator:wikidata",
            "network:wikidata",
            "flag:wikidata",
        )
    ):
        return None

    # ignore humans in operator tag
    if (
        link.key == "operator:wikidata"
        and Q_HUMAN in link.wd.all_instances_and_subclasses
    ):
        return None

    # ignore individual organism, memorals or natural monuments
    if (
        Q_INDIVIDUAL_ORGANISM in link.wd.all_instances_and_subclasses
        or Q_NATURAL_MONUMENT in link.wd.all_instances_and_subclasses
        or Q_MEMORIAL in link.wd.all_instances_and_subclasses
    ):
        return None

    # ignore if it is a direct instance of tree but not a taxon or organisms known by a particular common name
    if Q_TREE in link.wd.instances_of and not (
        Q_TAXON in link.wd.all_instances_and_subclasses
        or Q_ORGANISMS_KNOWN_BY_A_PARTICULAR_COMMON_NAME
        in link.wd.all_instances_and_subclasses
    ):
        return None

    issue = LivingOrganismIssue(link)
    issue.add_fix(DeleteFix(link.key))

    # non of these fix sugestion for streets
    if "highway" not in link.osm.tags:
        issue.add_fix(
            MultiFix(
                [
                    DeleteFix(link.key),
                    NewFix("subject:wikidata", "Q" + str(link.wd.qid)),
                ]
            )
        )
        issue.add_fix(
            MultiFix(
                [
                    DeleteFix(link.key),
                    NewFix("buried:wikidata", "Q" + str(link.wd.qid)),
                ]
            )
        )
        if Q_HUMAN not in link.wd.all_instances_and_subclasses:
            issue.add_fix(
                MultiFix(
                    [
                        DeleteFix(link.key),
                        NewFix("species:wikidata", "Q" + str(link.wd.qid)),
                    ]
                )
            )
            issue.add_fix(
                MultiFix(
                    [
                        DeleteFix(link.key),
                        NewFix("genus:wikidata", "Q" + str(link.wd.qid)),
                    ]
                )
            )

    issue.add_fix(
        MultiFix(
            [
                DeleteFix(link.key),
                NewFix("name:etymology:wikidata", "Q" + str(link.wd.qid)),
            ]
        )
    )

    return issue


def check_unpermitted_instance(
    link: Link,
) -> UnpermittedInstanceIssue | None:
    """checks for not allowed instance for this tag according to https://wiki.openstreetmap.org/wiki/Key:wikidata#Secondary_Wikidata_links"""

    # wikimedia internals, lists or web pages are never permitted (as direct instance)
    unpermitted_qids = (
        Q_WIKIMEDIA_DISAMBIGUATION_PAGE,
        Q_WIKIMEDIA_CATEGORY,
        Q_WIKIMEDIA_LIST_ARTICLE,
        Q_LIST,
    )

    for unpermitted_qid in unpermitted_qids:
        if unpermitted_qid in link.wd.instances_of:
            return UnpermittedInstanceIssue(
                link, unpermitted_qid, fix=DeleteFix(link.key)
            )

    return None


def check_places(link: Link) -> PlaceMismatchIssue | None:
    # Only check primary tags (and not e.g. 'name:etymology:wikidata') of Nodes an Areas
    if not isinstance(link, (NodeLink, AreaLink)) or not link.key == "wikidata":
        return None

    # maximal allowed distance multiplied by two giving all normal distances a >50% score
    places = {
        "country": 200.0 * 2,  # 0.0% of worldwide osm objects according to taginfo
        "state": 60.0 * 2,  # 0.0%
        "county": 20.0 * 2,  # 0.1%
        "city": 10.0 * 2,  # 0.3%
        "town": 10.0 * 2,  # 2%
        "suburb": 10.0 * 2,  # 2%
        "village": 10.0 * 2,  # 19%
        "hamlet": 10.0 * 2,  # 23%
        "locality": 10.0 * 2,  # 21%
        "neighbourhood": 10.0 * 2,  # 5%
    }

    max_area_distance_km = 2.0 * 2

    place = link.osm.tags.get("place", None)
    if not place in places:
        return None

    scores = {}

    # name score
    scores["name_score"] = c.score_names(link)

    # distance score
    if isinstance(link, AreaLink):
        max_distance = max_area_distance_km
    else:
        max_distance = places[place]
    if (distance := c.min_distance_km(link)) is not None:
        if distance > max_distance:
            scores["distance_score"] = 0.0
        else:
            scores["distance_score"] = (max_distance - distance) / max_distance
    else:
        scores["distance_score"] = None

    # postal code score
    scores["postal_code_score"] = c.score_postal_code(link)

    # calculate total score
    # but only if there are more than one score
    if sum(1 if v is not None else 0 for v in scores.values()) <= 1:
        return None
    # for unknow values (None's) assume 50%
    sum_ = sum(v if v is not None else 0.5 for v in scores.values())
    total_score = sum_ / len(scores)

    if total_score < 0.45:  # threshold
        return PlaceMismatchIssue(link, total_score, scores, DeleteFix(link.key))

    return None


def check_very_large_distance(link: Link) -> VeryLargeDistanceIssue | None:
    if link.key != "wikidata":  # just primary tags
        return None

    # exclute ways because of to many false positives (e.g. river parts, highway section)
    if isinstance(link.osm, OsmWay):
        return None

    # exclute river parts, because they could be fare away from wikidata where the source is estuary mouth is located
    if link.osm.tags.get("water", None) == "river":
        return None

    # exclute large places because representing point can have big differences
    if link.osm.tags.get("place", None) in ("continent", "ocean"):
        return None

    max_distance_km = 2000
    if (distance_km := c.min_distance_km(link)) is not None:
        if distance_km > max_distance_km:
            return VeryLargeDistanceIssue(link, distance_km, DeleteFix(link.key))
    return None


def check_primary_tag_instance_of(link: Link) -> PrimaryTagClaimMismatchIssue | None:
    # just primary tags
    if link.key != "wikidata":
        return None

    if len(link.wd.all_instances_and_subclasses) <= 1:
        return None

    for tag, value in link.osm.tags.items():
        if tag in PRIMARY_TAG_MATCHES:
            values = PRIMARY_TAG_MATCHES.get(tag, {})
            if value in values or "" in values:
                instances: Set[int] | None = values.get(value, None)
                instances_general: Set[int] | None = values.get("", None)
                if instances_general and len(instances_general) >= 0:
                    if instances:
                        instances |= instances_general
                    else:
                        instances = instances_general
                if not instances or len(instances) == 0:
                    continue
                overlapping_instances = link.wd.all_instances_and_subclasses & instances
                if len(overlapping_instances) == 0:
                    tag_cause = f"{tag}={value if value != '' else '*'}"
                    issue = PrimaryTagClaimMismatchIssue(
                        link, tag_cause, instances, fix=DeleteFix(link.key)
                    )

                    return issue
    return None


def check_secondary_tags_instance_of(
    link: Link,
) -> SecondaryTagClaimMismatchIssue | None:
    if not link.key.endswith(":wikidata"):
        return None

    if len(link.wd.all_instances_and_subclasses) <= 1:
        return None

    tag_key = link.key.replace(":wikidata", "")
    instances: Set[int] = SECONDARY_WIKIDATA_TAGS.get(tag_key, set())
    if len(instances) == 0:
        return None

    overlapping_instances = link.wd.all_instances_and_subclasses & instances
    if len(overlapping_instances) == 0:
        return SecondaryTagClaimMismatchIssue(link, instances, fix=DeleteFix(link.key))

    return None


def check_is_replaced_wikidata_item(link: Link) -> ReplacedWdItemIssue | None:
    if link.wd.replaced_by_qid is None or link.wd.replaced_by_qid == 0:
        return None
    # HINT: Ignore "replaced" munipalicities and administrative boundaries as this would generate to much Issues
    if (
        link.osm.tags.get("boundary", None) == "administrative"
        or link.osm.tags.get("place", None) == "village"
    ):
        return None
    fix = ChangeFix(link.key, "Q" + str(link.wd.replaced_by_qid))
    return ReplacedWdItemIssue(link, fix)


def check_dissolved_wd_item(link: Link) -> DissolvedWdItemIssue | None:
    if not link.wd.dissolved:
        return None
    # HINT: Ignore "replaced" munipalicities and administrative boundaries as this would generate to much Issues
    if (
        link.osm.tags.get("boundary", None) == "administrative"
        or link.osm.tags.get("place", None) == "village"
    ):
        return None
    return DissolvedWdItemIssue(link, fix=DeleteFix(link.key))
