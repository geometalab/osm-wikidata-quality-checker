import os

# TODO Deprecated module 'distutils.util' (deprecated-module)
from distutils.util import strtobool  # pylint: disable=deprecated-module
from pathlib import Path

from dotenv import load_dotenv


def _check_and_create_folder(path_2_folder):
    if not os.path.exists(path_2_folder):
        os.makedirs(path_2_folder, exist_ok=True)


# Load all env's
load_dotenv()

_OSM_KEY = "osm"
_WD_KEY = "wd"
_REDIRECTS_KEY = "redirects"
_NONE_KEYS = {"no", "none", "skip", "false"}

_DO_DOWNLOAD = set(
    os.getenv("DO_DOWNLOAD", f"{_OSM_KEY },{_WD_KEY },{_REDIRECTS_KEY }").split(",")
)
_DO_IMPORT = set(
    os.getenv("DO_IMPORT", f"{_OSM_KEY },{_WD_KEY },{_REDIRECTS_KEY }").split(",")
)

DO_CHECKS = bool(strtobool(os.getenv("DO_CHECKS", "True")))

DO_SEND_OSMOSE = bool(strtobool(os.getenv("DO_SEND_OSMOSE", "True")))
PATH_2_OSMOSE_REPORTS_FOLDER = os.getenv("PATH_2_OSMOSE_REPORTS_FOLDER", None)
USE_FILE_IDX_CACHE = bool(strtobool(os.getenv("USE_FILE_IDX_CACHE", "True")))


REPORT_FOLDER = Path(os.getenv("REPORT_FOLDER", "./reports"))
_DUMP_FOLDER = Path(os.getenv("DUMP_FOLDER", "./dumps"))
_DB_FOLDER = Path(os.getenv("DB_FOLDER", "./db"))
LOG_FOLDER = Path(os.getenv("LOG_FOLDER", "./logs"))
_check_and_create_folder(REPORT_FOLDER)  # TODO should be done in main
_check_and_create_folder(_DUMP_FOLDER)  # TODO should be done in main
_check_and_create_folder(_DB_FOLDER)  # TODO should be done in main
_check_and_create_folder(LOG_FOLDER)  # TODO should be done in main

WD_DUMP_URL = os.getenv(
    "WD_DUMP_URL",
    "https://dumps.wikimedia.org/wikidatawiki/entities/latest-all.json.bz2",
)
OSM_DUMP_URL = os.getenv(
    "OSM_DUMP_URL", "https://planet.openstreetmap.org/pbf/planet-latest.osm.pbf"
)
WD_REDIRECTS_URL = os.getenv("WD_REDIRECTS_URL", "https://query.wikidata.org/sparql")

_WD_DUMP_FILENAME = Path(os.getenv("WD_DUMP_FILENAME", "latest-all.json.bz2"))
_OSM_DUMP_FILENAME = Path(os.getenv("OSM_DUMP_FILENAME", "planet-latest.osm.pbf"))
_WD_REDIRECTS_FILENAME = Path(os.getenv("WD_REDIRECTS_FILENAME", "qredirects.csv"))

_OSM_DB_FILENAME = Path(os.getenv("OSM_DB_FILENAME", "osm.db"))
_WD_DB_FILENAME = Path(os.getenv("WD_DB_FILENAME", "wikidata.db"))
_WD_REDIRECTS_DB_FILENAME = Path(os.getenv("WD_REDIRECTS_DB_FILENAME", "redirects.db"))

# TODO defaults not zero
WD_DUMP_MIN_ALLOWED_FILESIZE = int(os.getenv("WD_DUMP_MIN_ALLOWED_FILESIZE", "0"))
OSM_DUMP_MIN_ALLOWED_FILESIZE = int(os.getenv("OSM_DUMP_MIN_ALLOWED_FILESIZE", "0"))

WD_DB_MIN_ALLOWED_FILESIZE = int(os.getenv("WD_DB_MIN_ALLOWED_FILESIZE", "0"))
OSM_DB_MIN_ALLOWED_FILESIZE = int(os.getenv("OSM_DB_MIN_ALLOWED_FILESIZE", "0"))

OSMOSE_API_HOST = os.getenv("OSMOSE_API_HOST", "https://osmose.openstreetmap.fr")
OSMOSE_SOURCE = os.getenv("OSMOSE_SOURCE", "")
OSMOSE_CODE = os.getenv("OSMOSE_CODE", "")

MP_CPU_THRESHOLD = int(os.getenv("MP_CPU_THRESHOLD", "4"))

ALERT_URL = os.getenv(
    "ALERT_URL",
    "https://gitlab.com/geometalab/osm-wikidata-quality-checker/alerts/notify/owqc-status/52b66d0804a3654c.json",
)
ALERT_AUTH = os.getenv("ALERT_AUTH", None)

INSTANCE_NAME = os.getenv("INSTANCE_NAME", "owqc")

#####################
# CALCULATED VALUES #
#####################

# TODO yes should also work
DO_WD_IMPORT = _WD_KEY in _DO_IMPORT and len(_DO_IMPORT & _NONE_KEYS) == 0
DO_OSM_IMPORT = _OSM_KEY in _DO_IMPORT and len(_DO_IMPORT & _NONE_KEYS) == 0
DO_WD_REDIRECTS_IMPORT = (
    _REDIRECTS_KEY in _DO_IMPORT and len(_DO_IMPORT & _NONE_KEYS) == 0
)

# TODO yes should also work
DO_WD_DOWNLOAD = _WD_KEY in _DO_DOWNLOAD and len(_DO_DOWNLOAD & _NONE_KEYS) == 0
DO_OSM_DOWNLOAD = _OSM_KEY in _DO_DOWNLOAD and len(_DO_DOWNLOAD & _NONE_KEYS) == 0
DO_WD_REDIRECTS_DOWNLOAD = (
    _REDIRECTS_KEY in _DO_DOWNLOAD and len(_DO_DOWNLOAD & _NONE_KEYS) == 0
)

PATH_2_OSM_DB = os.path.join(_DB_FOLDER, _OSM_DB_FILENAME)
PATH_2_WD_DB = os.path.join(_DB_FOLDER, _WD_DB_FILENAME)
PATH_2_WD_REDIRECTS_DB = os.path.join(_DB_FOLDER, _WD_REDIRECTS_DB_FILENAME)

PATH_2_WD_DUMP = os.path.join(_DUMP_FOLDER, _WD_DUMP_FILENAME)
PATH_2_WD_REDIRECTS = os.path.join(_DUMP_FOLDER, _WD_REDIRECTS_FILENAME)
PATH_2_OSM_DUMP = os.path.join(_DUMP_FOLDER, _OSM_DUMP_FILENAME)

PATH_2_OSMOSE_COUNTRY_CONFIG = os.getenv(
    "PATH_2_OSMOSE_COUNTRY_CONFIG",
    os.path.abspath(os.path.dirname(__file__)) + "/res",
)
