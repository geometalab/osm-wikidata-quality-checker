import logging

import requests
import tqdm


def download(url: str, path_2_file: str, progress_bar_pos: int, log=False):
    req = requests.get(url, stream=True)
    total_length = int(req.headers.get("content-length", 0))
    LOG_PARTS = 10
    log_chunk_size = total_length // LOG_PARTS
    log_next_chunk = log_chunk_size
    log_counter = 0
    with open(path_2_file, "wb") as file, tqdm.tqdm(
        desc=url,
        total=total_length,
        unit="b",
        unit_scale=True,
        unit_divisor=1024,
        position=progress_bar_pos,
    ) as pbar:
        for chunk in req.iter_content(chunk_size=2**24):
            if chunk:
                pbar.update(len(chunk))
                file.write(chunk)
                if log:
                    log_counter += len(chunk)
                    if log_counter >= log_next_chunk:
                        logging.info(
                            f"Downloaded {(log_next_chunk / log_chunk_size)/LOG_PARTS:.0%} of {path_2_file}"
                        )
                        log_next_chunk += log_chunk_size
