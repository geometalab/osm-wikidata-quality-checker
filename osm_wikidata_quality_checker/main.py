import logging
import os
import sys
import time
from datetime import datetime
from multiprocessing import JoinableQueue, Process, cpu_count
from typing import List, Type

from . import env
from .check_osmose_country_commit_hash import check_commit_hash
from .checker import check, mp_checker
from .checker_issues import CheckerIssue
from .database_common import do_databases_exist, remove_existing_database
from .downloader import download
from .gitlab_alerts import Severity, send_alert
from .osm_database import OsmArea, OsmDatabase, OsmNode, OsmObject, OsmRelation, OsmWay
from .osm_import import osm_import
from .osmose_reporter import OsmoseReporter
from .query_redirects import query_and_save_redirects
from .reporter import (
    CloseReport,
    CsvReporter,
    ExcelReporter,
    MultiReporter,
    SummaryReporter,
    mp_reporter,
)
from .wd_database import WdDatabase
from .wd_import import wd_import
from .wd_redirects_import import WdRedirectsDatabase, redirects_import

start_time = datetime.now()
START_TIME = start_time.strftime("%Y-%m-%d_%H-%M")

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s] [%(levelname)s] [%(processName)s] - %(message)s",
    handlers=[
        logging.FileHandler(f"{env.LOG_FOLDER}/log_{START_TIME}.log"),
        logging.StreamHandler(sys.stdout),
    ],
)


def _assert_min_allowed_filesize(path2file, min_allowed_filesize):
    filesize = os.stat(path2file).st_size
    if filesize < min_allowed_filesize:
        raise Exception(
            f"file size {path2file} is ({filesize}) smaller than allowed ({min_allowed_filesize})",
        )
    logging.info(
        "filesize check ok: %d >= %d for %s",
        filesize,
        min_allowed_filesize,
        path2file,
    )


# TODO should be in env.py
def _log_env_vars():
    for env_var, val in sorted(env.__dict__.items(), key=lambda kv: kv[0]):
        if env_var.isupper() and not env_var.startswith("_"):
            # hide secrets
            if env_var in ("ALERT_AUTH", "OSMOSE_CODE"):
                val = "<very-secret>" if val else "<empty-string>"
            logging.info("%s: '%s'", env_var, val)


def _get_multi_reporter():
    prefix = "report_" + START_TIME
    return MultiReporter(
        (
            CsvReporter(env.REPORT_FOLDER, prefix + ".csv"),
            OsmoseReporter(
                env.REPORT_FOLDER,
                "osmose_" + prefix,
                env.PATH_2_OSMOSE_COUNTRY_CONFIG,
                send_report=env.DO_SEND_OSMOSE,
            ),
            ExcelReporter(env.REPORT_FOLDER, prefix + ".xlsx", START_TIME),
            SummaryReporter(env.REPORT_FOLDER, "summary" + prefix + ".json"),
        )
    )


def _download_all_mp(do_osm: bool, do_wd: bool, do_wd_redirects: bool):
    logging.info("start download")
    download_processes: List[Process] = []
    i = 0
    if do_osm:
        p = Process(
            name="osm download",
            target=download,
            args=(env.OSM_DUMP_URL, env.PATH_2_OSM_DUMP, i, True),
        )
        p.start()
        download_processes.append(p)
        i += 1
    if do_wd:
        p = Process(
            name="wd download",
            target=download,
            args=(env.WD_DUMP_URL, env.PATH_2_WD_DUMP, i, True),
        )
        p.start()
        download_processes.append(p)
        i += 1
    if do_wd_redirects:
        p = Process(
            name="redirects download",
            target=query_and_save_redirects,
            args=(env.WD_REDIRECTS_URL, env.PATH_2_WD_REDIRECTS, i),
        )
        p.start()
        download_processes.append(p)
        i += 1

    for p in download_processes:
        p.join()
        if (code := p.exitcode) != 0:
            raise Exception(f"a download failed with exitcode {code}")

    logging.info("end download")


def _import_all_mp(do_osm: bool, do_wd: bool, do_redirects: bool):
    # pylint: disable=R0912
    logging.info(
        f"start import (osm={do_osm} / wd={do_wd}) / redirects={do_redirects})"
    )
    # Start sub-processes
    if do_osm:
        _assert_min_allowed_filesize(
            env.PATH_2_OSM_DUMP, env.OSM_DUMP_MIN_ALLOWED_FILESIZE
        )
        logging.info("start import osm dump")
        remove_existing_database(env.PATH_2_OSM_DB)
        osm_imp_proc = Process(
            name="osm import",
            target=osm_import,
            args=(env.PATH_2_OSM_DB, env.PATH_2_OSM_DUMP),
            kwargs={"use_file_idx_cache": env.USE_FILE_IDX_CACHE},
        )
        osm_imp_proc.start()

    if do_wd:
        _assert_min_allowed_filesize(
            env.PATH_2_WD_DUMP, env.WD_DUMP_MIN_ALLOWED_FILESIZE
        )
        logging.info("start import wd dump")
        remove_existing_database(env.PATH_2_WD_DB)
        wd_imp_proc = Process(
            name="wd import",
            target=wd_import,
            args=(env.PATH_2_WD_DB, env.PATH_2_WD_DUMP),
        )
        wd_imp_proc.start()

    if do_redirects:
        logging.info("start import wd redirects")
        remove_existing_database(env.PATH_2_WD_REDIRECTS_DB)
        redirects_imp_proc = Process(
            name="redirects import",
            target=redirects_import,
            args=(
                env.PATH_2_WD_REDIRECTS_DB,
                env.PATH_2_WD_REDIRECTS,
            ),
        )
        redirects_imp_proc.start()

    # Print status of sub-processes
    while any(
        [
            do_osm and osm_imp_proc.is_alive(),
            do_wd and wd_imp_proc.is_alive(),
            do_redirects and redirects_imp_proc.is_alive(),
        ]
    ):
        time.sleep(10 * 60)  # every 10min (wait first to allow progresses to start)
        if do_osm and osm_imp_proc.is_alive():
            size = os.stat(env.PATH_2_OSM_DB).st_size
            logging.info(
                f"osm import is running. File size: {size:16,} bytes ({size/env.OSM_DB_MIN_ALLOWED_FILESIZE if env.OSM_DB_MIN_ALLOWED_FILESIZE else 0:.1%} of allowed size)"
            )
        if do_wd and wd_imp_proc.is_alive():
            size = os.stat(env.PATH_2_WD_DB).st_size
            logging.info(
                f"wd import is running. File size: {os.stat(env.PATH_2_WD_DB).st_size:16,} bytes ({size/env.WD_DB_MIN_ALLOWED_FILESIZE if env.WD_DB_MIN_ALLOWED_FILESIZE else 0:.1%} of allowed size)"
            )
        if do_redirects and redirects_imp_proc.is_alive():
            logging.info(
                f"redirects import is running. File size: {os.stat(env.PATH_2_WD_REDIRECTS_DB).st_size:16,} bytes"
            )

    # Join sub_processes
    if do_redirects:
        redirects_imp_proc.join()
        if (code := redirects_imp_proc.exitcode) != 0:
            raise Exception(f"redirects import failed with exitcode {code}")
        logging.info(
            f"end import for wd redirects. File size: {os.stat(env.PATH_2_WD_REDIRECTS_DB).st_size:16,} bytes"
        )

    if do_wd:
        wd_imp_proc.join()
        if (code := wd_imp_proc.exitcode) != 0:
            raise Exception(f"wd import failed with exitcode {code}")
        logging.info(
            f"end import for wd dump. File size: {os.stat(env.PATH_2_WD_DB).st_size:16,} bytes"
        )

    if do_osm:
        osm_imp_proc.join()
        if (code := osm_imp_proc.exitcode) != 0:
            raise Exception(f"osm import failed with exitcode {code}")
        logging.info(
            f"end import osm dump. File size: {os.stat(env.PATH_2_OSM_DB).st_size:16,} bytes"
        )

    logging.info("end import")


def _check_all():
    _assert_min_allowed_filesize(env.PATH_2_OSM_DB, env.OSM_DB_MIN_ALLOWED_FILESIZE)
    _assert_min_allowed_filesize(env.PATH_2_WD_DB, env.WD_DB_MIN_ALLOWED_FILESIZE)
    logging.info("start check all")
    if not do_databases_exist(
        [env.PATH_2_OSM_DB, env.PATH_2_WD_DB, env.PATH_2_WD_REDIRECTS_DB]
    ):
        return
    osm_db = OsmDatabase(env.PATH_2_OSM_DB)
    wd_db = WdDatabase(env.PATH_2_WD_DB)
    wd_redirects_db = WdRedirectsDatabase(env.PATH_2_WD_REDIRECTS_DB)
    reporter = _get_multi_reporter()
    issues: List[CheckerIssue] = []

    for osm_objs in osm_db.get_all():
        for osm_obj in osm_objs:
            if issue := check(osm_obj, wd_db, wd_redirects_db):
                issues.append(issue)
        reporter.report(issues)
        reporter.flush()
        issues = []
    reporter.close()
    logging.info("end check all")


def _osm_obj_queue_filler(
    path_2_osm_db: str, queue: JoinableQueue, osm_type: Type[OsmObject]
):
    osm_db = OsmDatabase(path_2_osm_db)
    for osm_objs in osm_db.get_all_type(osm_type):
        queue.put(osm_objs)


def _check_all_mp():
    _assert_min_allowed_filesize(env.PATH_2_OSM_DB, env.OSM_DB_MIN_ALLOWED_FILESIZE)
    _assert_min_allowed_filesize(env.PATH_2_WD_DB, env.WD_DB_MIN_ALLOWED_FILESIZE)
    logging.info("start check all (mp)")
    if not do_databases_exist(
        [env.PATH_2_OSM_DB, env.PATH_2_WD_DB, env.PATH_2_WD_REDIRECTS_DB]
    ):
        return
    osm_obj_queue = JoinableQueue(100)
    issues_queue = JoinableQueue()
    check_process_count = cpu_count()
    osm_objs_processes: List[Process] = []
    checker_processes: List[Process] = []

    for osm_type in [OsmNode, OsmWay, OsmRelation, OsmArea]:
        process = Process(
            name=f"check queue filler ({str(osm_type)})",
            target=_osm_obj_queue_filler,
            args=(env.PATH_2_OSM_DB, osm_obj_queue, osm_type),
        )
        process.start()
        osm_objs_processes.append(process)

    for i in range(check_process_count):
        process = Process(
            name=f"checker ({i}))",
            target=mp_checker,
            args=(
                env.PATH_2_WD_DB,
                env.PATH_2_WD_REDIRECTS_DB,
                osm_obj_queue,
                issues_queue,
            ),
        )
        process.daemon = True
        process.start()
        checker_processes.append(process)

    reporter = _get_multi_reporter()
    reporter_processs = Process(
        name="reporter",
        target=mp_reporter,
        args=(
            reporter,
            issues_queue,
        ),
    )
    reporter_processs.daemon = True
    reporter_processs.start()

    # TODO check return codes for save failing
    for process in osm_objs_processes:
        process.join()

    osm_obj_queue.join()
    issues_queue.join()

    for process in checker_processes:
        process.terminate()
        process.join()

    issues_queue.put(CloseReport())
    issues_queue.join()

    reporter_processs.terminate()
    reporter_processs.join()
    logging.info("end check all (mp)")


def run():
    try:
        _log_env_vars()

        if not check_commit_hash():
            send_alert(
                "WARNING osmose country config commit hash outdated",
                "",
                Severity.Low,
            )

        # 1. DOWNLOAD
        if any([env.DO_OSM_DOWNLOAD, env.DO_WD_DOWNLOAD, env.DO_WD_REDIRECTS_DOWNLOAD]):
            send_alert("Start download", "")
            _download_all_mp(
                env.DO_OSM_DOWNLOAD, env.DO_WD_DOWNLOAD, env.DO_WD_REDIRECTS_DOWNLOAD
            )

        # 2. IMPORT
        if any([env.DO_OSM_IMPORT, env.DO_WD_IMPORT, env.DO_WD_REDIRECTS_IMPORT]):
            send_alert("Start import", "")
            _import_all_mp(
                env.DO_OSM_IMPORT, env.DO_WD_IMPORT, env.DO_WD_REDIRECTS_IMPORT
            )

        # 3. CHECK
        if env.DO_CHECKS:
            send_alert("Start check", "")
            # run mp check on server
            # TODO change thershold to use max. CPUS, not
            if cpu_count() >= env.MP_CPU_THRESHOLD:
                _check_all_mp()
            else:
                _check_all()

        # (4.) SEND OSMOSE ONLY
        if env.DO_SEND_OSMOSE and not any(
            [
                env.DO_OSM_DOWNLOAD,
                env.DO_WD_DOWNLOAD,
                env.DO_WD_REDIRECTS_DOWNLOAD,
                env.DO_OSM_IMPORT,
                env.DO_WD_IMPORT,
                env.DO_WD_REDIRECTS_IMPORT,
                env.DO_CHECKS,
            ]
        ):
            send_alert("Start send only to Osmose", "")
            # TODO move to osmose_reporter
            if not env.PATH_2_OSMOSE_REPORTS_FOLDER or not os.path.exists(
                env.PATH_2_OSMOSE_REPORTS_FOLDER
            ):
                raise Exception(
                    f"path for PATH_2_OSMOSE_REPORTS_FOLDER does not exits ({str(env.PATH_2_OSMOSE_REPORTS_FOLDER)})"
                )
            reporter = OsmoseReporter(
                env.REPORT_FOLDER,  # doesn't matter for send only, but must be valid
                "send_only_dummy",  # doesn't matter for send only, but must be valid
                env.PATH_2_OSMOSE_COUNTRY_CONFIG,
                send_report=env.DO_SEND_OSMOSE,
            )
            reporter.send_reports(env.PATH_2_OSMOSE_REPORTS_FOLDER)

        # END
        send_alert("End run", "")
        logging.info("End run successfully")

    except Exception as e:
        send_alert(
            f"Run terminated with {type(e).__name__}",
            str(e),
            severity=Severity.Critical,
        )
        logging.exception("Run terminated with %s: %s", type(e).__name__, str(e))
        raise  # to end programm and print traceback to stderr etc..
    finally:
        d = datetime.now() - start_time
        logging.info(f"total runtime: {d.days} days / {d.seconds/60/60:.1f} hours")
