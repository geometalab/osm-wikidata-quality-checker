import logging
import tempfile
from multiprocessing import JoinableQueue, Process
from time import sleep
from typing import Callable, List, Type

from osmium import SimpleHandler, geom

from .import_common import (
    DEFAULT_BULK_SIZE,
    DEFAULT_QUEUE_MAX,
    InsertBuffer,
    queue_to_database,
)
from .osm_database import OsmArea, OsmDatabase, OsmNode, OsmRelation, OsmWay


class _OsmDumpPbfReader(SimpleHandler):
    def __init__(
        self,
        insert_function: Callable,
    ):
        SimpleHandler.__init__(self)
        self.insert_function = insert_function


class _OsmDumpPbfReaderWayArea(_OsmDumpPbfReader):
    def __init__(
        self,
        insert_function: Callable,
    ):
        super().__init__(insert_function)
        self.wkbfab = geom.WKBFactory()

    def area(self, area):
        wkb = None
        if "wikidata" in "".join([t.k for t in area.tags]):
            tags = {t.k: t.v for t in area.tags}

            num_rings_outer, num_rings_inner = area.num_rings()
            # Ausschliessen wenn: num_rings_inner==0 && num_rings_outer==0
            # Da es sonst ein Import-Error gibt
            if num_rings_inner == 0 and num_rings_outer == 0:
                return

            wkb = self._create_area_wkb(area)

            osm_obj = OsmArea()
            osm_obj.oid = area.orig_id()
            osm_obj.uid = area.id
            osm_obj.tags = tags
            osm_obj.user = area.user if len(area.user) > 0 else None
            osm_obj.version = area.version
            osm_obj.is_way = area.from_way()
            osm_obj.is_multipolygon = area.is_multipolygon()
            osm_obj.num_rings_inner = num_rings_inner
            osm_obj.num_rings_outer = num_rings_outer
            osm_obj._wkb_hex = wkb  # pylint: disable=protected-access

            self.insert_function(osm_obj)

    def _create_area_wkb(self, area):
        wkb = None
        try:
            wkb = self.wkbfab.create_multipolygon(area)
        except Exception as ex:
            logging.error(
                "can not create multipolygon wkb for area odi %s, id %s, exception: %s",
                area.orig_id(),
                area.id,
                ex,
            )
        return wkb

    def way(self, way):
        if "wikidata" in "".join([t.k for t in way.tags]):
            tags = {t.k: t.v for t in way.tags}
            is_closed = way.is_closed()
            # if area is closed, and area-tag is not "no", the way will be handled as area
            if is_closed and tags.get("area", "yes") != "no":
                return

            wkb = None
            try:
                wkb = self.wkbfab.create_linestring(way)
            except Exception as ex:
                logging.error(
                    "can not create linestring wkb for way id %s, exception: %s",
                    way.id,
                    ex,
                )

            osm_obj = OsmWay()
            osm_obj.oid = way.id
            osm_obj.tags = tags
            osm_obj.user = way.user if len(way.user) > 0 else None
            osm_obj.version = way.version
            osm_obj.is_closed = is_closed
            osm_obj._wkb_hex = wkb  # pylint: disable=protected-access

            self.insert_function(osm_obj)


class _OsmDumpPbfReaderNode(_OsmDumpPbfReader):
    def node(self, node):
        if "wikidata" in "".join([t.k for t in node.tags]):
            tags = {t.k: t.v for t in node.tags}
            osm_obj = OsmNode()
            osm_obj.oid = node.id
            osm_obj.tags = tags
            osm_obj.user = node.user if len(node.user) > 0 else None
            osm_obj.version = node.version
            osm_obj.set_persistant_location(node.location.lat, node.location.lon)

            self.insert_function(osm_obj)


class _OsmDumpPbfReaderRelation(_OsmDumpPbfReader):
    def relation(self, relation):
        # if type is 'boundary' or 'multipolygon', then the relation will be handled as area
        if relation.tags.get("type") in {"boundary", "multipolygon"}:
            return

        if "wikidata" in "".join([t.k for t in relation.tags]):
            tags = {t.k: t.v for t in relation.tags}
            members = [(r.ref, r.role, r.type) for r in relation.members]

            osm_obj = OsmRelation()
            osm_obj.oid = relation.id
            osm_obj.tags = tags
            osm_obj.user = relation.user if len(relation.user) > 0 else None
            osm_obj.version = relation.version
            osm_obj.members = members
            self.insert_function(osm_obj)


class _OsmDumpPbfReaderAll(
    _OsmDumpPbfReaderNode,
    _OsmDumpPbfReaderRelation,
    _OsmDumpPbfReaderWayArea,
):  # pylint: disable=too-many-ancestors
    pass


def _get_index_cache(use_file) -> str:
    # info https://osmcode.org/osmium-concepts/#indexes
    if use_file:
        cache_file = tempfile.NamedTemporaryFile()
        return "dense_file_array," + cache_file.name
    return "flex_mem"


def _import_osm_type(
    path_2_osm_dump,
    osm_pbf_reader: Type[_OsmDumpPbfReader | _OsmDumpPbfReaderWayArea],
    idx: str,
    queue: JoinableQueue,
    bulk_size: int = DEFAULT_BULK_SIZE,
):
    try:
        buffer: InsertBuffer = InsertBuffer(queue.put, bulk_size)
        reader: _OsmDumpPbfReader | _OsmDumpPbfReaderWayArea | None = None
        # HINT actualy type determination is done with 'is', the 'isinstnace' is only for mypy
        if osm_pbf_reader is _OsmDumpPbfReaderWayArea and isinstance(
            osm_pbf_reader, type(_OsmDumpPbfReaderWayArea)
        ):
            reader = osm_pbf_reader(buffer.insert)
        elif isinstance(osm_pbf_reader, type(_OsmDumpPbfReader)):
            reader = osm_pbf_reader(buffer.insert)
        assert reader is not None
        reader.apply_file(path_2_osm_dump, idx=idx)
        buffer.flush()
    except Exception as e:
        logging.exception(
            "Exception in _import_osm_type (%s): %s", str(osm_pbf_reader), str(e)
        )
        raise


def osm_import(
    path_2_db: str,
    path_2_dump: str,
    bulk_size: int = DEFAULT_BULK_SIZE,
    use_multiprocessing: bool = True,
    use_file_idx_cache: bool = True,
):

    try:
        if use_multiprocessing:
            _osm_import_mp(path_2_db, path_2_dump, bulk_size, use_file_idx_cache)
        else:
            database = OsmDatabase(path_2_db)
            buffer: InsertBuffer = InsertBuffer(database.add_all, bulk_size)
            reader = _OsmDumpPbfReaderAll(buffer.insert)
            reader.apply_file(path_2_dump, idx=_get_index_cache(use_file_idx_cache))
            buffer.flush()
    except Exception as e:
        logging.exception("Exception in osm_import: %s", str(e))
        raise


def _osm_import_mp(
    path_2_db, path_2_dump, bulk_size, use_file_idx_cache, join_sleep_time=10
):
    queue: JoinableQueue = JoinableQueue(maxsize=DEFAULT_QUEUE_MAX)
    db_import_process = Process(
        name="osm queue to database",
        target=queue_to_database,
        args=(queue, OsmDatabase, path_2_db),
        daemon=True,
    )
    db_import_process.start()

    osm_import_processes: List[Process] = []

    osm_import_processes.append(
        Process(
            # name=f"osm import {type(_OsmDumpPbfReaderNode).__name__}",
            name="osm import _OsmDumpPbfReaderNode",
            target=_import_osm_type,
            args=(
                path_2_dump,
                _OsmDumpPbfReaderNode,
                _get_index_cache(False),
                queue,
                bulk_size,
            ),
        )
    )
    osm_import_processes.append(
        Process(
            name="osm import _OsmDumpPbfReaderRelation",
            target=_import_osm_type,
            args=(
                path_2_dump,
                _OsmDumpPbfReaderRelation,
                _get_index_cache(False),
                queue,
                bulk_size,
            ),
        )
    )
    osm_import_processes.append(
        Process(
            name="osm import _OsmDumpPbfReaderWayArea",
            target=_import_osm_type,
            args=(
                path_2_dump,
                _OsmDumpPbfReaderWayArea,
                _get_index_cache(use_file_idx_cache),
                queue,
                bulk_size,
            ),
        )
    )

    for p in osm_import_processes:
        p.start()

    # wait for import processes to end. if any process crashes, terminate all processes
    crashed_process = None
    while not crashed_process and len(osm_import_processes) != 0:
        sleep(join_sleep_time)
        # check osm import processes to end or crash
        # pylint: disable=modified-iterating-list
        for p in osm_import_processes:
            if not p.is_alive():
                if p.exitcode != 0:  # crash
                    crashed_process = p
                    break
                # else ended normally
                osm_import_processes.remove(p)
        # check db import process
        if not db_import_process.is_alive():
            crashed_process = db_import_process

    if crashed_process:
        # terminate all processes
        for p in osm_import_processes + [db_import_process]:
            p.terminate()
        raise Exception(
            f"osm import process '{crashed_process.name}' failed with exitcode {crashed_process.exitcode}"
        )

    # wait unitl queue is empty, then stop db importer
    queue.join()
    db_import_process.terminate()
    db_import_process.join()
    if (code := db_import_process.exitcode) != -15:  # SIGTERM due to terminate()
        raise Exception(f"subprocess db_inserter failed with exitcode {code}")
