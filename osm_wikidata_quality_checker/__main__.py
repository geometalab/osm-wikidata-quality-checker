# Note that the minor version is not up to date because we continuously merge development versions into master without updating this versions string:
__version__ = "0.1.0"
import os
import sys

from .main import run

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

run()
