import csv
import gzip
import json
import logging
import os
from dataclasses import dataclass, field
from datetime import datetime
from pathlib import Path
from typing import Dict, Iterable, List, Type
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import Element, SubElement

import requests
import shapely
import shapely.wkt
from shapely.geometry import MultiPolygon, Point

from .checker_issues import *  # pylint: disable=wildcard-import,W0614
from .env import OSMOSE_API_HOST, OSMOSE_CODE, OSMOSE_SOURCE
from .reporter import Reporter


@dataclass(frozen=True)
class OsmoseErrorDetails:
    # for more details see https://github.com/osm-fr/osmose-backend/blob/master/doc/0-Index.md#classes-of-issues
    id: int
    title: str
    detail: str
    fix: str | None = None
    trap: str | None = None
    example: str | None = None
    tag: str = "wikidata, tag, fixme"
    item: int = 3031
    level: int = 2
    source: str = "https://gitlab.com/geometalab/osm-wikidata-quality-checker"


_ISSUE_2_OSMOSE_CLASS_MAPPING: Dict[Type[CheckerIssue], OsmoseErrorDetails] = {
    InvalidWdItemFormatIssue: OsmoseErrorDetails(
        id=1,
        title="Incorrect value for Wikidata-Tag",
        detail="The format of the Wikidata-ID is wrong. Only valid values like `Q123` oder multiple items like `Q123;Q124` are allowed. Lexems like `L123` are also allowed for tags with `*:etymology:wikidata`, but ignored. See [Wikidata](https://wiki.openstreetmap.org/wiki/Wikidata) and [Key:wikidata](https://wiki.openstreetmap.org/wiki/Key:wikidata) for more information.",
        fix="If you see a wrongly formatted value, check if it matches the OSM object and correct it. Otherwise delete the tag.",
    ),
    NotExistingWdItemIssue: OsmoseErrorDetails(
        id=2,
        title="Wikidata item does not exist",
        detail="The linked Wikidata item was not found in the Wikidata database dump.",
        fix="Check if the tag realy does not exists, then delete the affected tag.",
        trap="We used the Wikidata database dump to check for this error. It is possible that there are Items which are not yet in the used dump, therefore false errors can be generated for very new items. Always make sure that it realy does not exist.",
    ),
    RedirectedWdItemIssue: OsmoseErrorDetails(
        id=3,
        title="Redirected value for Wikidata tag",
        detail="This Wikidata item is a redirect, you shoud change the QID to the redirect destionation.",
        fix="Change the value of the affected tag to the suggested new Wikidata-ID.",
    ),
    VeryLargeDistanceIssue: OsmoseErrorDetails(
        id=4,
        title="The distance between OSM object and linked Wikidata item is unusually large",
        detail="The shortest distance between the OSM object and the linked Wikidata item is very large, this hints to a incorectly linked Wikidata item.",
        fix="Verify that the link is in fact incorrect, then correct or delete the tag. It could also be that the qualifier is set wrongly, so that a prefix like `brand:wikidata`, `subject:wikidata` or `is_in:wikidata` fixes the problem. For more details on qualifier see [Secondary Wikidata links](https://wiki.openstreetmap.org/wiki/Key:wikidata#Secondary_Wikidata_links)",
    ),
    PrimaryTagClaimMismatchIssue: OsmoseErrorDetails(
        id=5,
        title="Characteristics of the OSM tags and linked Wikidata item do not match",
        detail="OSM tags do not match the `instance-of` and/or `subclass-of` claims in the Wikidata item.",
        example="An OSM object with tag `railway=*` should be an instance or subclass of `Transport infrastructure (Q350783)`. Or the operator of something (e.g. industrial factory) is specified in the primary wikidata tag (`wikidata=`), but should be specified as a secondary wikidata tag (`operator:wikidata=`).",
        fix="Either remove the tag or correct it by linking it to an matching Wikidata item. It could also be that you should use a secondary Wikidata link instead. Sometimes, however, the information on Wikidata is incomplete and should be corrected there.",
    ),
    SecondaryTagClaimMismatchIssue: OsmoseErrorDetails(
        id=6,
        title="The secondary Wikidata tag and the linked Wikidata item do not match",
        detail="The Wikidata tag prefix does not match the `instance-of` and/or `subclass-of` claims in the Wikidata item. Fore more details see [Secondary Wikidata links](https://wiki.openstreetmap.org/wiki/Key:wikidata#Secondary_Wikidata_links).",
        example="The secondary Wikidata tag `brand:wikidata` should be a instance or subclass of `Brand (Q431289)`, `Trademark (Q167270)` or `Organisation (Q43229)`.",
        fix="Remove the affected tag or change to another secondary Wikidata tag, for example use `brand:wikidata` instead of `operator:wikidata`. Or add the required `instance-of` or `subclass-of` claims to the Wikidata item.",
    ),
    UnpermittedInstanceIssue: OsmoseErrorDetails(
        id=7,
        title="The OSM object is linked to an unpermitted Wikidata item",
        detail="Only items that directly relate to the feature should be linked, do not link to Wikidata internals, lists or discussion pages. See [Key:Wikidata](https://wiki.openstreetmap.org/wiki/Key:wikidata) for more details.",
        example="A specific statue is linked to an Wikidata item representing a list of statues.",
        fix="Remove the incorect tag (or change it to the specific Wikidata item).",
    ),
    LivingOrganismIssue: OsmoseErrorDetails(
        id=8,
        title="Unpermitted link to an instance of living organism on Wikidata",
        detail="It is not allowed to link wikidata tags `wikidata`, `brand:wikidata`, `operator:wikidata` (humans allowed), `network:wikidata` or `flag:wikidata` directly to a living organism (human, animal, plant). Use a suitable secondary Wikidata link for this purpose. For more details see [Key:Wikidata](https://wiki.openstreetmap.org/wiki/Key:wikidata).",
        example="The name origin of a street name is marked with the tag `wikidata` instead of `name:etymology:wikidata`. The Human buried at a burial site is tag with the `wikidata` tag instead of `buried:wikidata`.",
        fix="Remove the incorect tag and/or apply one of the suggested tags.",
    ),
    PlaceMismatchIssue: OsmoseErrorDetails(
        id=9,
        title="The OSM object does not match the Wikidata item",
        detail="The name and/or distance and/or postal code found in the OSM tag and Wikidata claims do not match sufficiently.",
        fix="Verfy that the link is in fact incorrect, then correct or delete the tag.",
        trap="Sometimes claims on Wikidata, like coordinates, are just too inaccurate.",
    ),
    ReplacedWdItemIssue: OsmoseErrorDetails(
        id=10,
        title="Linked Wikidata item is replaced by other item",
        detail="This Wikidata item is replaced by another Wikidata item, you shoud check the validity of this Wikidata item and consider to change the QID to the 'replaced by' destionation.",
        fix="Check if this Wikidata item is still valid, if it is actually replaced by the other Wikidata item, set the new item as Value of the affected tag.",
    ),
    DissolvedWdItemIssue: OsmoseErrorDetails(
        id=11,
        title="Linked Wikidata item is dissolved, abolished or demolished",
        detail="Wikidata item has a 'dissolved, abolished or demolished date' and should therefore not exist anymore.",
        fix="Check if the real Thing behind the Wikidata item does in fact no longer exist. If so, remove this Wikidata Link from OSM.",
    ),
}


def _get_osmose_class(issue: CheckerIssue) -> OsmoseErrorDetails:
    return _ISSUE_2_OSMOSE_CLASS_MAPPING[type(issue)]


@dataclass
class _CountryAnalyser:
    boundary_oids: List[int]
    country_code: str
    name: str
    geoms: List[MultiPolygon]
    issues: List[CheckerIssue] = field(default_factory=list)


def _build_country_analysers(
    path_2_osmose_country_config: str,
) -> List[_CountryAnalyser]:
    try:
        config_path = os.path.join(
            path_2_osmose_country_config, "osmose_country_config.json"
        )
        with open(config_path, "rt") as f:
            country_config = json.load(f)

        wkt_path = os.path.join(
            path_2_osmose_country_config, "osmose_country_wkt.json.gz"
        )
        with gzip.open(wkt_path, "rt") as f:
            country_wkts = json.load(f)

        analysers = []
        for country, values in country_config.items():
            geoms = []
            oids = values["boundary_ids"]
            for oid in oids:
                wkt_ = country_wkts.get(str(oid))
                if not wkt_:
                    logging.warning(
                        "no wkt found for osmose country %s (%s) with oid=%s",
                        country,
                        country["country_code"],
                        oid,
                    )
                    continue
                try:
                    geoms.append(shapely.wkt.loads(wkt_))
                except Exception as e:
                    logging.error(
                        f"could not load wkt for country {country} with oid{oid}: {e}"
                    )
            if not geoms:
                logging.warning(
                    "empty geometries for osmose country %s (%s)",
                    country,
                    country["country_code"],
                )
            analyser = _CountryAnalyser(oids, values["country_code"], country, geoms)
            analysers.append(analyser)
        return analysers
    except Exception as e:
        logging.error(f"exepton in _build_country_analysers: {e}")
        raise


class OsmoseReporter(Reporter):
    def __init__(
        self,
        path: Path,
        prefix: str,
        path_2_osmose_country_config: str,
        api_host: str = OSMOSE_API_HOST,
        api_source: str = OSMOSE_SOURCE,
        api_code: str = OSMOSE_CODE,
        send_report: bool = True,
    ):
        logging.info("start Osmose report")
        time = datetime.now()
        self.xlm_timestamp = time.strftime("%Y-%m-%dT%H:%M:%SZ")
        self.path_2_reports_folder = os.path.join(path, prefix)
        self.api_host: str = api_host
        self.api_source: str = api_source
        self.api_code: str = api_code
        self.do_send_report = send_report
        self.fallback_issues: List[CheckerIssue] = []
        self._country_analysers: List[_CountryAnalyser] = _build_country_analysers(
            path_2_osmose_country_config
        )
        self._no_location_relation_counter = 0

    def open(self):
        pass

    def report(self, issues: List[CheckerIssue]):
        for issue in issues:
            self._add_to_country_analyser(issue)

    def _add_to_country_analyser(self, issue: CheckerIssue):
        issue_location = issue.osm.get_location()

        if not issue_location:
            # HACK currently, there are no locations for (non-area) relations
            if not isinstance(issue.osm, OsmRelation):
                logging.error(
                    "no location for issue subclass: %s osm_type: %s osm_id: %s tag: %s",
                    issue.hash_id,
                    issue.osm.get_osm_type_name(),
                    issue.osm.oid,
                    issue.key,
                )
            else:
                self._no_location_relation_counter += 1
            return

        location_point = Point(issue_location.lon, issue_location.lat)
        for analyser in self._country_analysers:
            for oid, geom in zip(analyser.boundary_oids, analyser.geoms):
                try:
                    if location_point.within(geom) or location_point.touches(geom):
                        analyser.issues.append(issue)
                        return
                except Exception as e:
                    logging.error(
                        f"Exception within analyser {analyser.name} ({analyser.country_code} oid={oid}) while cheking location of {issue.osm.get_osm_type_name()} {issue.osm.oid} : {e}"
                    )

        # no country analyser found, add to fallback list
        self.fallback_issues.append(issue)

    def flush(self):
        pass

    def close(self):
        logging.info(
            f"skipped relations with no coordinates: {self._no_location_relation_counter}"
        )
        self._save_reports()
        if self.do_send_report:
            self.send_reports()
        else:
            logging.info("no report is send (DO_SEND_OSMOSE)")
        logging.info("end Osmose reporter")

    def _save_fallbacks(self):
        if self.fallback_issues:
            logging.warning(
                f"no country analyser found for {len(self.fallback_issues)} issue"
            )
            path = os.path.join(self.path_2_reports_folder, "fallback_issues.csv")
            with open(path, "w", newline="", encoding="utf8") as f:
                writer = csv.writer(f, dialect="excel")
                writer.writerow(
                    ["oid", "osm type", "abstract type", "issue type", "lat", "lon"]
                )
                for i in self.fallback_issues:
                    loc = i.osm.get_location()
                    writer.writerow(
                        [
                            i.osm.oid,
                            i.osm.get_osm_type_name(),
                            i.osm.get_abstract_type_name(),
                            type(i),
                            loc.lat if loc else "",
                            loc.lon if loc else "",
                        ]
                    )

    def _save_reports(self):
        logging.info("start save Osmose reports to %s", self.path_2_reports_folder)
        os.makedirs(self.path_2_reports_folder)
        for country_anaylser in self._country_analysers:
            path_2_report = os.path.join(
                self.path_2_reports_folder,
                self._build_report_filename(
                    country_anaylser.country_code, country_anaylser.name
                ),
            )
            if os.path.exists(path_2_report):
                raise Exception(
                    "path to report '{country_anaylser.name}' does already exist: {path_2_report}"
                )
            with open(
                path_2_report, "w", newline="", encoding="US-ASCII"
            ) as report_file:
                report_file.write(self._build_xml(country_anaylser))
                report_file.flush()
        self._save_fallbacks()
        logging.info("end save Osmose reports")

    def _build_report_filename(self, code: str, name: str) -> str:
        return f"report_{code}_{name}.xml"

    def send_reports(self, path_2_reports_folder: str | None = None):
        logging.info("start sending reports to Osmose")
        if not self.api_host:
            logging.error("api_host is empty. No reprots sent")
            return
        not_ok_count = 0
        if path_2_reports_folder is None:
            path_2_reports_folder = self.path_2_reports_folder
        for country_anaylser in self._country_analysers:
            path_2_report = os.path.join(
                path_2_reports_folder,
                self._build_report_filename(
                    country_anaylser.country_code, country_anaylser.name
                ),
            )
            if not os.path.exists(path_2_report):
                logging.error(
                    "path to report '%s' does not exist: %s",
                    country_anaylser.name,
                    path_2_report,
                )
                continue
            if not self._send_report(country_anaylser.name, path_2_report):
                not_ok_count += 1
        if not_ok_count == 0:
            logging.info(
                "end sending reports to Osmose (all %d reports sent)",
                len(self._country_analysers),
            )
        else:
            logging.error(
                "end sending reports (%d out of %d reports could not be sent)",
                not_ok_count,
                len(self._country_analysers),
            )

    def _send_report(self, country_name, path_2_report) -> bool:
        # logging.info("send xml report to Osmose for country %s", country)
        req = None
        url = f"{self.api_host}/control/send-update"

        try:
            with open(path_2_report, "rb") as report_file:
                files = {
                    "analyser": (None, self.api_source),
                    "country": (None, country_name),
                    "code": (None, self.api_code),
                    "content": (path_2_report, report_file),
                }
                req = requests.post(url, files=files)

            if not req.ok:
                logging.error(
                    "send xml to Osmose for country '%s' retuned status code (%s %s) and detail: %s",
                    country_name,
                    req.status_code,
                    req.reason,
                    req.json().get("detail", "<no detail>"),
                )

            return req.ok
        except Exception as e:
            logging.error(
                "send xml report to Osmose for country %s failed due to %s: %s",
                country_name,
                type(e).__name__,
                str(e),
            )
            return False

    def _build_xml(
        self,
        country_analyser: _CountryAnalyser,
    ) -> str:
        timestamp_attr = {"timestamp": self.xlm_timestamp}
        root = Element("analysers", attrib=timestamp_attr)

        root.append(self._build_analyser(country_analyser.issues))

        return ET.tostring(
            root, encoding="US-ASCII", method="xml", xml_declaration=True
        ).decode()

    def _build_analyser(self, issues: List[CheckerIssue]):
        analyser = Element(
            "analyser",
            attrib={"timestamp": self.xlm_timestamp},
        )
        analyser.extend(self._build_classes())
        analyser.extend(self._build_errors(issues))
        return analyser

    @staticmethod
    def _build_classes() -> List[Element]:
        """
        Class definition (many)
        id: referenced by <error class="" />
        item: osmose frontend target item https://github.com/osm-fr/osmose-frontend/blob/759c208d180ae0ccca242e1559191f94ef8cadd4/tools/database/items_menu.txt
        level: matter of issue in OSM data, from "1" as major to "3" as minor
        tag: key-words of issue (not a set of OSM tags) https://osmose.openstreetmap.fr/api/0.3/tags
        title of class, one line per language
        """

        # used_classes = {_get_osmose_class(issue) for issue in issues}
        used_classes = _ISSUE_2_OSMOSE_CLASS_MAPPING.values()
        classes = []
        for cls in used_classes:
            class_elem = Element(
                "class",
                attrib={
                    "item": str(cls.item),
                    "tag": cls.tag,
                    "id": str(cls.id),
                    "level": str(cls.level),
                    "source": cls.source,
                },
            )
            SubElement(
                class_elem,
                "classtext",
                attrib={
                    "lang": "en",
                    "title": cls.title,
                },
            )

            further_class_info = [
                ("detail", cls.detail),
                ("fix", cls.fix),
                ("trap", cls.trap),
                ("example", cls.example),
            ]

            for element, value in further_class_info:
                if value:
                    SubElement(
                        class_elem,
                        element,
                        attrib={
                            "lang": "en",
                            "title": value,
                        },
                    )

            classes.append(class_elem)

        return classes

    @staticmethod
    def _get_fix(fix: AllFixTypes, osm_type: str, oid: int) -> Element:
        fix_elem = Element("fix")

        osm_elem = SubElement(fix_elem, osm_type, attrib={"id": str(oid)})

        fixes = [fix] if isinstance(fix, Fix) else fix.fixes
        for f in fixes:
            tag_elem_attrib = {"k": f.key, "action": "delete"}

            if isinstance(f, ValueFix):
                tag_elem_attrib["v"] = f.value
                if isinstance(f, ChangeFix):
                    tag_elem_attrib["action"] = "modify"
                else:
                    tag_elem_attrib["action"] = "create"

            SubElement(osm_elem, "tag", attrib=tag_elem_attrib)

        return fix_elem

    def _build_errors(self, issues: Iterable[CheckerIssue]) -> List[Element]:
        """
        Issue entry (many)
        class: refer to <class id="" />
        subclass: group the same kind of issue (optional)
        location: Issue location marker
        node: erroneous object (none or many), all details are optional
        text: optional subtitle, one line per language
        """
        errors: List[Element] = []
        for issue in issues:
            cls = _get_osmose_class(issue)
            osm_type = issue.osm.get_osm_type_name()

            issue_location = issue.osm.get_location()

            error = Element(
                "error", attrib={"class": str(cls.id), "subclass": str(issue.hash_id)}
            )

            if issue_location:
                SubElement(
                    error,
                    "location",
                    attrib={
                        "lat": str(issue_location.lat),
                        "lon": str(issue_location.lon),
                    },
                )

            error_obj_type = SubElement(
                error,
                osm_type,
                attrib={
                    "id": str(issue.osm.oid),
                    "user": str(issue.osm.user),
                    "version": str(issue.osm.version),
                    "lat": str(issue_location.lat) if issue_location else "",
                    "lon": str(issue_location.lon) if issue_location else "",
                },
            )

            SubElement(
                error_obj_type,
                "tag",
                attrib={"k": issue.key, "v": issue.osm.tags.get(issue.key, "")},
            )

            if issue.text:
                SubElement(
                    error,
                    "text",
                    attrib={
                        "lang": "en",
                        "value": issue.text,
                    },
                )

            if len(issue.fixes) > 0:
                fixes = SubElement(error, "fixes")
                for fix in issue.fixes:
                    fixes.append(self._get_fix(fix, osm_type, issue.osm.oid))

            errors.append(error)
        return errors
