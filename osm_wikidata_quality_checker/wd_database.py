# pylint: disable=protected-access
from typing import List, Set

from sqlalchemy import Column, Float, ForeignKey, Integer, String, select
from sqlalchemy.orm import (
    Mapped,
    Session,
    declarative_base,
    joinedload,
    reconstructor,
    relationship,
)

from .database_common import (
    AbstractDatabase,
    JSONEncodedDict,
    JSONEncodedList,
    JSONEncodedSet,
)

_Wd_Base = declarative_base()


class WdItem(_Wd_Base):
    __tablename__ = "item"

    qid: Mapped[int] = Column(Integer, primary_key=True)
    names: Mapped[dict] = Column(JSONEncodedDict)
    country_qid = Column(Integer)
    replaced_by_qid = Column(Integer)
    dissolved = Column(String)
    instances_of: Mapped[set] = Column(JSONEncodedSet)
    subclasses_of: Mapped[set] = Column(JSONEncodedSet)
    street_addresses: Mapped[list] = Column(JSONEncodedList)
    postal_codes: Mapped[list] = Column(JSONEncodedList)
    located_on_streets: Mapped[list] = Column(JSONEncodedList)
    coordinates: Mapped[List["WdCoordinate"]] = relationship("WdCoordinate")

    def __init__(self) -> None:
        super().__init__()
        self.all_instances_and_subclasses: Set[int] = set()

    @reconstructor
    def init_on_load(self):
        self.all_instances_and_subclasses: Set[int] = set()  # type: ignore

    def __repr__(self) -> str:
        return f"WdItem(qid={self.qid})"


class WdCoordinate(_Wd_Base):
    __tablename__ = "coordinate"
    _id = Column(Integer, primary_key=True)
    qid = Column(Integer, ForeignKey("item.qid"), index=True)
    lat = Column(Float)
    lon = Column(Float)
    _item: WdItem = relationship("WdItem", back_populates="coordinates")

    def __init__(self, lat, lon):
        self.lat = lat
        self.lon = lon

    def __repr__(self) -> str:
        return f"WdCoordinate(id={self._id}, qid={self.qid})"


class WdDatabase(AbstractDatabase):
    def __init__(self, path_2_db):
        super().__init__(path_2_db)
        self._init(_Wd_Base)

    def add_all(self, objs: List[WdItem]):
        with Session(self.engine) as session:
            session.add_all(objs)
            session.commit()

    def _get_recursive_instance_of(
        self, session: Session, to_load: Set[int], loaded: Set[int]
    ) -> Set[int]:

        loaded = loaded | to_load
        sub_to_load: Set[int] = set()

        rows = session.query(WdItem.instances_of, WdItem.subclasses_of).where(
            WdItem.qid.in_(to_load)
        )
        for row in rows:
            sub_to_load = sub_to_load | ((row[0] | row[1]) - loaded)

        if len(sub_to_load) > 0:
            loaded = loaded | self._get_recursive_instance_of(
                session, sub_to_load, loaded
            )

        return loaded

    def get(self, qid: int) -> WdItem | None:
        stmt = (
            select(WdItem)
            .where(WdItem.qid == qid)
            .options(joinedload(WdItem.coordinates))
        )
        with Session(self.engine) as session:
            wd_item = session.scalars(stmt).first()
            if isinstance(wd_item, WdItem):
                if wd_item.instances_of is None:
                    wd_item.instances_of = set()
                if wd_item.subclasses_of is None:
                    wd_item.subclasses_of = set()
                wd_item.all_instances_and_subclasses = (
                    wd_item.instances_of | wd_item.subclasses_of
                )
                wd_item.all_instances_and_subclasses = self._get_recursive_instance_of(
                    session, wd_item.all_instances_and_subclasses, {wd_item.qid}
                )
            return wd_item
