import re

import requests
import tqdm


def _query(url, query):
    r = requests.post(
        url,  # "https://query.wikidata.org/sparql"
        params={"query": query},
        headers={"Accept": "text/tab-separated-values"},
    )
    return r.text.splitlines()


_entity_re = re.compile(r"^<http://www.wikidata.org/entity/([A-Z]\d+)>$")

_timestamp_re = re.compile(
    r'^"(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z)"\^\^<http://www.w3.org/2001/XMLSchema#dateTime>$'
)


def _query_redirect_chunk(url, offset, limit):
    # pylint: disable=consider-using-f-string
    query = """
    SELECT ?a ?b ?modified WHERE {
         SERVICE bd:slice {
             ?a owl:sameAs ?b .
             bd:serviceParam bd:slice.offset %d .
             bd:serviceParam bd:slice.limit %d .
         }
         ?a schema:dateModified ?modified .
    }""" % (
        offset,
        limit,
    )
    for line in _query(url, query)[1:]:
        c = line.split("\t")
        src = _entity_re.search(c[0]).group(1)
        target = _entity_re.search(c[1]).group(1)
        timestamp = _timestamp_re.search(c[2]).group(1)
        yield (src, target, timestamp)


def _count_redirect(url):
    return int(_query(url, "SELECT (COUNT(*) as ?c) WHERE {[] owl:sameAs [].}")[1])


def _query_redirects(url: str, progress_bar_pos=None):
    count = _count_redirect(url)
    limit = 100000
    print(count)
    with tqdm.tqdm(total=count, position=progress_bar_pos) as pbar:
        for off in range(0, count, limit):
            for src, target, timestamp in _query_redirect_chunk(url, off, limit):
                yield (src, target, timestamp)
                pbar.update(limit)


def query_and_save_redirects(url: str, path: str, progress_bar_pos=None):
    with open(path, "w", encoding="utf-8") as f:
        f.writelines(
            map(lambda x: ",".join(x) + "\n", _query_redirects(url, progress_bar_pos))
        )


# if __name__ == "__main__":
#     # for src, target, timestamp in _query_redirects():
#     # print(src, target, timestamp)
#     query_and_save_redirects(
#         "https://query.wikidata.org/sparql",
#         "./osm_wikidata_quality_checker/res/test_redirects.csv",
#     )
