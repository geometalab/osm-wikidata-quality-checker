from typing import Iterable, Set

import numpy as np
from fuzzywuzzy import fuzz
from shapely.geometry import LineString, Point, Polygon
from shapely.ops import nearest_points

from .checker_issues import AreaLink, Link, NodeLink, RelationLink, WayLink


def _haversine_distance_km(lat1, lon1, lat2, lon2) -> float:
    # calculates difference in km https://towardsdatascience.com/heres-how-to-calculate-distance-between-2-geolocations-in-python-93ecab5bbba4
    radius = 6371
    phi1 = np.radians(lat1)
    phi2 = np.radians(lat2)
    delta_phi = np.radians(lat2 - lat1)
    delta_lambda = np.radians(lon2 - lon1)
    a = (
        np.sin(delta_phi / 2) ** 2
        + np.cos(phi1) * np.cos(phi2) * np.sin(delta_lambda / 2) ** 2
    )
    res = radius * (2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a)))
    return np.round(res, 2)


def _best_strings_ratio(
    strings1: Iterable[str], strings2: Iterable[str]
) -> float | None:
    if not strings1 or not strings2:
        return None
    max_score = 0
    for n1 in strings1:
        for n2 in strings2:
            score = fuzz.ratio(n1.lower(), n2.lower())  # als test gegen .ratio
            max_score = max(max_score, score)
    return max_score / 100


def _string_token_set_ratio(string1: str, string2: str) -> float | None:
    if not string1 or not string2:
        return None
    score = fuzz.token_set_ratio(string1, string2)
    return score / 100


def _min_distance_shape_to_points_km(
    shape: Polygon | LineString, points: list[Point]
) -> float:
    distances = []

    for point in points:
        if point.within(shape):
            distances.append(0.0)
            break
        nearest_point = nearest_points(shape, point)
        distance = _haversine_distance_km(
            nearest_point[0].y, nearest_point[0].x, point.y, point.x
        )
        distances.append(distance)
        if distance == 0:
            break
    return min(distances)


def _wd_has_coordinates(link) -> bool:
    return link.wd.coordinates and len(link.wd.coordinates) > 0


def score_postal_code(link: Link) -> float | None:
    osm_postal = link.osm.tags.get("postal_code")
    wd_postals = link.wd.postal_codes
    if osm_postal and wd_postals:
        return max(fuzz.ratio(osm_postal, wd_postal) for wd_postal in wd_postals) / 100
    return None


def score_names(link: Link) -> float | None:
    osm = link.osm
    wd = link.wd
    WHITELIST = (
        "name",
        "alt_name",
        "loc_name",
        "official_name",
        "short_name",
        "short_name",
        "old_name",
    )
    BLACKLIST = ("etymology", "wikidata", "pronunciation", "description")

    osm_names = {
        v
        for k, v in osm.tags.items()
        if k.startswith(WHITELIST) and not any(x in k for x in BLACKLIST)
    }

    wd_names: Set[str] = set()
    for name in wd.names.values():
        wd_names.add(name)
        # also add names without without postfixes. In Wikipedia, the region is often added as postfix like "Helguera (Molledo)"
        if (pos := name.find(",")) != -1:
            wd_names.add(name[:pos].strip())
        if (pos := name.find("(")) != -1:
            wd_names.add(name[:pos].strip())

    return _best_strings_ratio(osm_names, wd_names)


def min_node_or_relation_distance_km(link: NodeLink | RelationLink) -> float | None:
    if not _wd_has_coordinates(link):
        return None

    if osm_loc := link.osm.get_location():
        return min(
            _haversine_distance_km(osm_loc.lat, osm_loc.lon, wd_coord.lat, wd_coord.lon)
            for wd_coord in link.wd.coordinates
        )
    return None


def min_osm_with_shape_distance(link: AreaLink | WayLink) -> float | None:
    if not _wd_has_coordinates(link):
        return None

    if shape := link.osm.get_shape():
        return _min_distance_shape_to_points_km(
            shape,
            [Point(wd_coord.lon, wd_coord.lat) for wd_coord in link.wd.coordinates],
        )
    return None


def min_distance_km(link: Link) -> float | None:
    # pylint: disable=no-else-return
    if isinstance(link, (NodeLink, RelationLink)):
        return min_node_or_relation_distance_km(link)
    elif isinstance(link, (AreaLink, WayLink)):
        return min_osm_with_shape_distance(link)
    else:
        raise NotImplementedError()
