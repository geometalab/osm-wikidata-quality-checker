from abc import ABC
from dataclasses import dataclass
from typing import Dict, List, Set

from .osm_database import OsmArea, OsmNode, OsmObject, OsmRelation, OsmWay
from .wd_database import WdItem


@dataclass
class Link(ABC):
    osm: OsmObject
    wd: WdItem
    key: str


@dataclass
class NodeLink(Link):
    osm: OsmNode


@dataclass
class WayLink(Link):
    osm: OsmWay


@dataclass
class RelationLink(Link):
    osm: OsmRelation


@dataclass
class AreaLink(Link):
    osm: OsmArea


def create_link(osm: OsmObject, wd: WdItem, key: str) -> Link:
    if isinstance(osm, OsmNode):
        return NodeLink(osm, wd, key)  # pylint: disable=arguments-out-of-order
    if isinstance(osm, OsmWay):
        return WayLink(osm, wd, key)  # pylint: disable=arguments-out-of-order
    if isinstance(osm, OsmRelation):
        return RelationLink(osm, wd, key)  # pylint: disable=arguments-out-of-order
    if isinstance(osm, OsmArea):
        return AreaLink(osm, wd, key)  # pylint: disable=arguments-out-of-order
    raise TypeError()


@dataclass
class Fix(ABC):
    key: str


@dataclass
class MultiFix:
    fixes: List[Fix]


@dataclass
class DeleteFix(Fix):
    pass


@dataclass
class ValueFix(Fix, ABC):
    value: str


@dataclass
class ChangeFix(ValueFix):
    pass


@dataclass
class NewFix(ValueFix):
    pass


AllFixTypes = Fix | MultiFix


class CheckerIssue(ABC):
    def __init__(
        self,
        osm: OsmObject,
        key: str,
        qid: int,
        text: str | None = None,
        fix: AllFixTypes | None = None,
    ):
        self.osm: OsmObject = osm
        self.key: str = key
        self.qid: int = qid
        self.text: str | None = text

        self.fixes: List[AllFixTypes] = []
        if fix:
            self.add_fix(fix)

        self.hash_id: int = 0
        self._create_hash(f"{self.key}:{self.qid}")

    def _create_hash(self, to_hash: str):
        self.hash_id = hash(to_hash) % (2**32)

    def add_fix(self, fix: AllFixTypes):
        self.fixes.append(fix)

    def get_type_name(self) -> str:
        return type(self).__name__


class LinkIssue(CheckerIssue):
    def __init__(
        self,
        link: Link,
        fix: AllFixTypes | None = None,
        text: str | None = None,
    ):
        super().__init__(link.osm, link.key, link.wd.qid, fix=fix, text=text)


class InvalidWdItemFormatIssue(CheckerIssue):
    def __init__(
        self, osm: OsmObject, key: str, invalid_qid: str, fix: AllFixTypes | None = None
    ):
        text = f"Wikidata-Tag has an invalid value: {invalid_qid if len(invalid_qid)<=24 else invalid_qid[:24]+'...'}"
        super().__init__(osm, key, 0, text, fix)
        self._create_hash(f"{self.key}:{invalid_qid}")


class NotExistingWdItemIssue(CheckerIssue):
    def __init__(
        self,
        osm: OsmObject,
        key: str,
        qid: int,
        fix: AllFixTypes | None = None,
    ):
        text = f"The linked Wikidata item Q{qid} does not exists on Wikidata"
        super().__init__(osm, key, qid, text, fix)


class RedirectedWdItemIssue(CheckerIssue):
    def __init__(
        self,
        osm: OsmObject,
        key: str,
        qid: int,
        redirected_to: str,
        fix: AllFixTypes | None = None,
    ):
        text = f"Wikidata item is redirected to {redirected_to}."
        super().__init__(osm, key, qid, text, fix)


class ReplacedWdItemIssue(LinkIssue):
    pass


class VeryLargeDistanceIssue(LinkIssue):
    def __init__(self, link: Link, distance_km: float, fix: AllFixTypes | None = None):
        text = f"distance difference: {distance_km:.1f}km"
        super().__init__(link, fix, text)
        self.distance_km = distance_km


class PrimaryTagClaimMismatchIssue(LinkIssue):
    def __init__(
        self,
        link: Link,
        tag_cause: str,
        instances: Set[int],
        fix: AllFixTypes | None = None,
    ):
        text = (
            f"OSM object has tag {tag_cause}, the linked wikidata item shoud therefore be an instance or subclass of one of those: "
            + ", ".join(f"Q{id}" for id in instances)
        )
        super().__init__(link, fix, text)


class SecondaryTagClaimMismatchIssue(LinkIssue):
    def __init__(self, link: Link, instances: Set[int], fix: AllFixTypes | None = None):
        text = (
            f"For a {link.key} tag, the linked wikidata item shoud be an instance or subclass of one of those: "
            + ", ".join(f"Q{id}" for id in instances)
        )
        super().__init__(link, fix, text)


class UnpermittedInstanceIssue(LinkIssue):
    def __init__(
        self,
        link: Link,
        unpermitted_instance_qid: int,
        fix: AllFixTypes | None = None,
    ):
        text = f"The linked Wikidata item is instance of Q{unpermitted_instance_qid}, which is not permitted"
        super().__init__(link, fix, text)
        self.unpermitted_instance_qid = unpermitted_instance_qid


class LivingOrganismIssue(LinkIssue):
    def __init__(
        self,
        link: Link,
        fix: AllFixTypes | None = None,
    ):
        text = "The linked Wikidata item is instance of a living organism (Q21871294), which is not permitted"
        super().__init__(link, fix, text)


class PlaceMismatchIssue(LinkIssue):
    def __init__(
        self,
        link: Link,
        total_score: float,
        scores: Dict[str, float | None],
        fix: AllFixTypes | None,
    ):
        scores_str = " | ".join(
            f"{k}:{v:.2f}" for k, v in scores.items() if v is not None
        )
        text = f"total score is to low and therefore possible a wrong: {total_score:.2f} ({scores_str})"
        super().__init__(link, fix, text=text)


class DissolvedWdItemIssue(LinkIssue):
    pass
