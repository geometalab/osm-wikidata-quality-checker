from typing import List

from sqlalchemy import Column, Integer, select
from sqlalchemy.orm import Mapped, Session, declarative_base

from .database_common import AbstractDatabase

_Wd_Base = declarative_base()


class WdRedirect(_Wd_Base):
    __tablename__ = "redirect"

    qid_from: Mapped[int] = Column(Integer, primary_key=True)
    qid_to: Mapped[int] = Column(Integer)

    def __init__(self, qid_from, qid_to):
        self.qid_from = qid_from
        self.qid_to = qid_to

    def __repr__(self) -> str:
        return f"WdRedirect(from={self.qid_from}/to={self.qid_to}"


class WdRedirectsDatabase(AbstractDatabase):
    def __init__(self, path_2_db):
        super().__init__(path_2_db)
        self._init(_Wd_Base)

    def add_all(self, objs: List[WdRedirect]):
        with Session(self.engine) as session:
            session.bulk_save_objects(objs, preserve_order=False)
            session.commit()

    def get(self, qid_from: int) -> WdRedirect | None:
        stmt = select(WdRedirect).where(WdRedirect.qid_from == qid_from)
        with Session(self.engine) as session:
            return session.scalars(stmt).first()
