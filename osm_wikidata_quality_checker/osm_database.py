import logging
from abc import abstractmethod
from dataclasses import dataclass
from typing import Dict, Generic, Iterator, List, Type, TypeVar

import shapely.wkb as wkblib
from shapely.geometry import LineString, Polygon
from sqlalchemy import Boolean, Column, Float, Integer, String, select
from sqlalchemy.ext.declarative import AbstractConcreteBase
from sqlalchemy.orm import Mapped, Session, declarative_base, reconstructor

from .database_common import AbstractDatabase, JSONEncodedDict, JSONEncodedList

_Osm_Base = declarative_base()


@dataclass
class Location:
    lat: float
    lon: float

    def __repr__(self) -> str:
        return f"Location(lat={self.lat}, lon={self.lon})"


class OsmObject(AbstractConcreteBase, _Osm_Base):
    oid: int
    tags: Dict
    user: str | None
    version: int

    def __init__(self):
        super().__init__()
        self._location: Location | None = None

    @reconstructor
    def init_on_load(self):
        self._location: Location | None = None

    @abstractmethod
    def get_abstract_type_name(self) -> str:
        pass

    @abstractmethod
    def get_osm_type_name(self) -> str:
        pass

    @abstractmethod
    def get_location(self) -> Location | None:
        pass


class OsmNode(OsmObject):
    __tablename__ = "node"
    __mapper_args__ = {"polymorphic_identity": "node", "concrete": True}

    oid = Column(Integer, primary_key=True)
    tags: Mapped[dict] = Column(JSONEncodedDict)
    user = Column(String)
    version = Column(Integer)
    _lat: Mapped[float] = Column(Float)
    _lon: Mapped[float] = Column(Float)

    def __init__(self):
        super().__init__()
        self._location: Location = Location(lat=self._lat, lon=self._lon)

    @reconstructor
    def init_on_load(self):
        self._location: Location = Location(lat=self._lat, lon=self._lon)

    def __repr__(self) -> str:
        return f"OsmNode(oid={self.oid})"

    def get_abstract_type_name(self) -> str:
        return "Node"

    def get_osm_type_name(self) -> str:
        return "node"

    def set_persistant_location(self, lat: float, lon: float):
        self._lat = lat
        self._lon = lon
        self._location = Location(lat=self._lat, lon=self._lon)

    def get_persistant_location(self):
        return (self._lat, self._lon)

    def get_location(self) -> Location:
        return self._location


ShapeType = TypeVar("ShapeType", Polygon, LineString)


class OsmObjWithShape(OsmObject, Generic[ShapeType]):
    __abstract__ = True

    _wkb_hex = Column(String)

    def __init__(self) -> None:
        super().__init__()
        self._shape: ShapeType | None = None
        self._shape_created: bool = False

    @reconstructor
    def init_on_load(self):
        super().init_on_load()
        self._shape: ShapeType | None = None
        self._shape_created: bool = False

    @abstractmethod
    def get_abstract_type_name(self) -> str:
        pass

    @abstractmethod
    def get_osm_type_name(self) -> str:
        pass

    def _create_shape(self):
        if not self._shape_created and self._wkb_hex is not None:
            try:
                self._shape = wkblib.loads(self._wkb_hex, hex=True)
            except Exception as exception:
                logging.error(
                    "shape can not be created for object oid: %d (%s)",
                    self.oid,
                    exception,
                )
                self._shape = None
            self._shape_created = True

    def get_shape(self) -> ShapeType:
        self._create_shape()
        return self._shape

    def get_location(self) -> Location | None:
        if not self._location:
            self._create_shape()
            if self._shape:
                self._location = Location(
                    self._shape.centroid.y,  # pylint: disable=no-member
                    self._shape.centroid.x,  # pylint: disable=no-member
                )
        return self._location


class OsmWay(OsmObjWithShape[LineString]):
    __tablename__ = "way"
    __mapper_args__ = {"polymorphic_identity": "way", "concrete": True}

    oid = Column(Integer, primary_key=True)
    tags: Mapped[dict] = Column(JSONEncodedDict)
    user = Column(String)
    version = Column(Integer)
    is_closed = Column(Boolean)
    _wkb_hex = Column(String)

    def __repr__(self) -> str:
        return f"OsmWay(oid={self.oid})"

    def get_abstract_type_name(self) -> str:
        return "Way"

    def get_osm_type_name(self) -> str:
        return "way"


class OsmRelation(OsmObject):
    __tablename__ = "relation"
    __mapper_args__ = {"polymorphic_identity": "relation", "concrete": True}

    oid = Column(Integer, primary_key=True)
    tags: Mapped[Dict] = Column(JSONEncodedDict)
    user = Column(String)
    version = Column(Integer)
    members: Mapped[List] = Column(JSONEncodedList)

    def __init__(self) -> None:
        super().__init__()
        self._location: Location | None = None

    @reconstructor
    def init_on_load(self):
        super().init_on_load()
        self._location: Location | None = None

    def __repr__(self) -> str:
        return f"OsmRelation(oid={self.oid})"

    def get_abstract_type_name(self) -> str:
        return "Relation"

    def get_osm_type_name(self) -> str:
        return "relation"

    def get_location(self) -> Location | None:
        return self._location

    def set_location(self, location: Location | None):
        self._location = location


class OsmArea(OsmObjWithShape[Polygon]):
    __tablename__ = "area"
    __mapper_args__ = {"polymorphic_identity": "area", "concrete": True}

    uid = Column(Integer, primary_key=True)
    oid = Column(Integer)
    tags: Mapped[dict] = Column(JSONEncodedDict)
    user = Column(String)
    version = Column(Integer)
    is_way = Column(Boolean)
    is_multipolygon = Column(Boolean)
    _wkb_hex = Column(String)
    num_rings_outer = Column(Integer)
    num_rings_inner = Column(Integer)

    def __repr__(self) -> str:
        return f"OsmArea(uid={self.uid} / oid={self.oid} / is_way={self.is_way} / is_multipolygon={self.is_multipolygon})"

    def get_abstract_type_name(self) -> str:
        if self.is_way:
            return "Way-Area"
        return "Relation-Area"

    def get_osm_type_name(self) -> str:
        if self.is_way:
            return "way"
        return "relation"


_DEFAULT_BULK_SIZE = 1000


class OsmDatabase(AbstractDatabase):
    def __init__(self, path_2_db):
        super().__init__(path_2_db)
        self._init(_Osm_Base)

    def add_all(self, objs: List[OsmObject]):
        with Session(self.engine) as session:
            session.bulk_save_objects(objs, preserve_order=False)
            session.commit()

    def get_all(self, bulk_size=_DEFAULT_BULK_SIZE) -> Iterator[List]:
        types = [OsmNode, OsmWay, OsmRelation, OsmArea]
        with Session(self.engine) as session:
            for osm_type in types:
                stmt = select(osm_type).execution_options(yield_per=bulk_size)
                yield from session.scalars(stmt).partitions(bulk_size)

    OsmObjectT = TypeVar("OsmObjectT", OsmNode, OsmRelation, OsmArea, OsmWay)

    def _get(self, oid: int, type_: Type[OsmObjectT]) -> OsmObjectT | None:
        with Session(self.engine) as session:
            stmt = select(type_).where(type_.oid == oid)
            return session.scalars(stmt).first()

    def get_node(self, oid: int) -> OsmNode | None:
        return self._get(oid, OsmNode)

    def get_way(self, oid: int) -> OsmWay | None:
        return self._get(oid, OsmWay)

    def get_relation(self, oid: int) -> OsmRelation | None:
        return self._get(oid, OsmRelation)

    def get_area(self, uid: int) -> OsmArea | None:
        with Session(self.engine) as session:
            s = select(OsmArea).where(OsmArea.uid == uid)
            return session.scalars(s).first()

    def get_all_type(
        self, select_type: Type[OsmObject], bulk_size=_DEFAULT_BULK_SIZE
    ) -> Iterator[List]:
        stmt = select(select_type).execution_options(yield_per=bulk_size)
        with Session(self.engine) as session:
            yield from session.scalars(stmt).partitions(bulk_size)
