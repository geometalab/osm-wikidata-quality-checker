import csv
import json
import os
from abc import ABC, abstractmethod
from datetime import datetime
from io import TextIOWrapper
from itertools import count
from multiprocessing.dummy import JoinableQueue
from pathlib import Path
from typing import Any, Iterable, List

import xlsxwriter

from .checker_issues import CheckerIssue


class Reporter(ABC):
    @abstractmethod
    def open(self):
        pass

    @abstractmethod
    def report(self, issues: List[CheckerIssue]):
        pass

    @abstractmethod
    def flush(self):
        pass

    @abstractmethod
    def close(self):
        pass


class CloseReport:
    pass


class MultiReporter(Reporter):
    def __init__(self, reporter: Iterable[Reporter]):
        self.reporters = reporter

    def open(self):
        for reporter in self.reporters:
            reporter.open()

    def report(self, issues: List[CheckerIssue]):
        for reporter in self.reporters:
            reporter.report(issues)

    def flush(self):
        for reporter in self.reporters:
            reporter.flush()

    def close(self):
        for reporter in self.reporters:
            reporter.close()


class SummaryReporter(Reporter):
    def __init__(self, path: Path, filename: str):
        self.path_2_report = os.path.join(path, filename)
        self.summary: Any = {"total": 0, "categories": {}}

    def open(self):
        pass

    def report(self, issues: List[CheckerIssue]):
        for issue in issues:
            name = issue.get_type_name()
            num = self.summary["categories"].get(name, 0)
            self.summary["categories"][name] = num + 1

    def flush(self):
        pass

    def close(self):
        self.summary["total"] = sum(self.summary["categories"].values())
        with open(self.path_2_report, "wt") as f:
            json.dump(self.summary, f, indent=4)


class CsvReporter(Reporter):
    def __init__(self, path: Path, filename: str):
        self.path_2_report = os.path.join(path, filename)
        self._report_file: TextIOWrapper | None = None

    def open(self):
        self._report_file = open(self.path_2_report, "w+", newline="", encoding="utf8")

        self._get_writer().writerow(
            ["issue_type", "osm_type", "osm_id", "osm_tag_key", "wd_qid", "text"]
        )
        self.flush()

    def _get_writer(self):
        if self._report_file is None or self._report_file.closed:
            self.open()
        return csv.writer(self._report_file, dialect="excel")

    def __del__(self):
        if self._report_file and not self._report_file.closed:
            self._report_file.close()

    def report(self, issues: List[CheckerIssue]):
        self._get_writer().writerows(
            [
                [
                    issue.get_type_name(),
                    issue.osm.get_abstract_type_name(),
                    issue.osm.oid,
                    issue.key,
                    issue.qid,
                    issue.text,
                ]
                for issue in issues
            ]
        )

    def flush(self):
        self._report_file.flush()

    def close(self):
        if not self._report_file.closed:
            self._report_file.close()


class ExcelReporter(Reporter):
    def __init__(self, path: Path, filename: str, timestamp: str | None = None):
        self.path_2_report = os.path.join(path, filename)
        self._filename = filename
        self._workbook: xlsxwriter.Workbook | None = None
        self._worksheet = None
        self._timestamp = timestamp
        self._currrent_row = 0

    def _write_header(self):
        ws = self._worksheet

        # initiation
        timestamp = self._timestamp if self._timestamp else str(datetime.now())
        initiation = (
            "OpenStreetmap Wikidata Quality Checker Issue Report",
            timestamp,
            self._filename,
        )

        for row, value in enumerate(initiation):
            ws.write(row, 0, value)

        # issue headings
        bold_f = self._workbook.add_format({"bold": True})
        for col, value in enumerate(
            (
                "issue_type",
                "osm_type",
                "osm_id",
                "osm_tag_key",
                "wd_qid",
                "text",
            )
        ):
            ws.write(len(initiation) + 1, col, value, bold_f)

        self._currrent_row = len(initiation) + 2

        # colum settings
        for col, value in enumerate(
            (
                30,
                10,
                10,
                10,
                10,
                10,
            )
        ):
            ws.set_column(col, col, width=value)

        self.flush()

    def open(self):
        self._workbook = xlsxwriter.Workbook(self.path_2_report)
        self._worksheet = self._workbook.add_worksheet("issues")
        self._write_header()

    def _get_writer(self):
        if not self._workbook:
            self.open()
        return self._worksheet

    def __del__(self):
        if self._workbook:
            self._workbook.close()

    @staticmethod
    def _link_formula(url: str, text: str) -> str:
        return f'=HYPERLINK("{url}", {text})'

    def report(self, issues: List[CheckerIssue]):
        ws = self._get_writer()
        link_format = (
            self._workbook.add_format({"underline": 1, "font_color": "blue"})
            if self._workbook
            else None
        )
        for issue in issues:
            col = count(0)
            ws.write(self._currrent_row, next(col), issue.get_type_name())
            ws.write(self._currrent_row, next(col), issue.osm.get_abstract_type_name())
            # NOTE: Don't use 'write_url' function as excel has a limit of hard-coded links. Use hyperlink-formula instead. https://stackoverflow.com/questions/41669690/how-to-overcome-the-limit-of-hyperlinks-in-excel
            ws.write(
                self._currrent_row,
                next(col),
                self._link_formula(
                    f"https://www.openstreetmap.org/{issue.osm.get_osm_type_name()}/{issue.osm.oid}",
                    str(issue.osm.oid),
                ),
                link_format,
            )
            ws.write(self._currrent_row, next(col), issue.key)
            ws.write(
                self._currrent_row,
                next(col),
                self._link_formula(
                    f"https://www.wikidata.org/wiki/Q{issue.qid}", str(issue.qid)
                ),
                link_format,
            )
            ws.write(self._currrent_row, next(col), issue.text)
            self._currrent_row += 1

    def flush(self):
        pass

    def close(self):
        if self._workbook:
            self._workbook.close()
            self._workbook = None


def mp_reporter(
    reporter: Reporter,
    issue_queue: JoinableQueue,
):
    reporter.open()
    while True:
        chunk: List[CheckerIssue] | CloseReport = issue_queue.get()
        if isinstance(chunk, CloseReport):
            reporter.flush()
            reporter.close()
        else:
            reporter.report(chunk)
            reporter.flush()
        issue_queue.task_done()
