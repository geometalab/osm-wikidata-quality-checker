#!/bin/bash
# Cron job to start the owqc and log
set -e
# make sure logfile is same as in loop from cron_setup.sh
log_path="/data/logs/cron_job.log"
dt=$(date)
# exec >> $log_path
# exec 2>&1
echo "#######################" >> $log_path
echo "$dt cron script started" >> $log_path
cd /app
python3 -m osm_wikidata_quality_checker 2> $log_path
echo -e "script ended successfully\n\n" >> $log_path
