import pytest

from osm_wikidata_quality_checker.wd_redirects_database import (
    WdRedirect,
    WdRedirectsDatabase,
)


@pytest.fixture
def database():
    return WdRedirectsDatabase(":memory:")


def test_add_and_get(database: WdRedirectsDatabase):
    redir1 = WdRedirect(10, 11)

    redir2 = WdRedirect(20, 21)

    redir3 = WdRedirect(30, 31)

    database.add_all([redir1, redir2, redir3])

    actual1 = database.get(10)
    actual2 = database.get(20)
    actual3 = database.get(30)

    assert actual1
    assert actual1.qid_from == 10
    assert actual1.qid_to == 11

    assert actual2
    assert actual2.qid_from == 20
    assert actual2.qid_to == 21

    assert actual3
    assert actual3.qid_from == 30
    assert actual3.qid_to == 31
