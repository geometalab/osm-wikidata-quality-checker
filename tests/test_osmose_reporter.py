import os

import pytest
import requests
from pytest_httpserver import HTTPServer, httpserver

from osm_wikidata_quality_checker.checker_issues import RedirectedWdItemIssue
from osm_wikidata_quality_checker.osm_database import Location, OsmNode
from osm_wikidata_quality_checker.osmose_reporter import (
    _ISSUE_2_OSMOSE_CLASS_MAPPING,
    InvalidWdItemFormatIssue,
    OsmoseErrorDetails,
    OsmoseReporter,
    _CountryAnalyser,
    _get_osmose_class,
)

# from http.server import HTTPServer


def test_unique_class_id():
    assert (
        len(_ISSUE_2_OSMOSE_CLASS_MAPPING) != 0
    ), "issue to osmose class mapping missing"
    ids = [v.id for v in _ISSUE_2_OSMOSE_CLASS_MAPPING.values()]
    assert len(ids) == len(set(ids)), "osmose class id's are not unique"


def test_get_osmose_class(invalidWdItemFormatIssue, redirectedWdItemIssue):
    assert isinstance(
        _get_osmose_class(invalidWdItemFormatIssue), OsmoseErrorDetails
    ), f"class actual {InvalidWdItemFormatIssue} / class shuld {OsmoseErrorDetails}"

    assert isinstance(
        _get_osmose_class(redirectedWdItemIssue), OsmoseErrorDetails
    ), f"class actual {RedirectedWdItemIssue} / class shuld {OsmoseErrorDetails}"


def test_country_analyser(tmpdir, invalidWdItemFormatIssue, redirectedWdItemIssue):
    prefix = "osmose_output_folder"
    reporter = OsmoseReporter(
        tmpdir,
        prefix,
        "tests/res/test_andora",
        send_report=False,
    )

    invalidWdItemFormatIssue.osm._location = Location(0.1, 0.1)  # out of andorra
    redirectedWdItemIssue.osm = OsmNode()  # attach new node necessary
    redirectedWdItemIssue.osm._location = Location(42.5, 1.5)  # inside andorra

    reporter.report([invalidWdItemFormatIssue, redirectedWdItemIssue])

    assert len(reporter._country_analysers) == 1
    assert len(reporter._country_analysers[0].issues) == 1
    assert type(reporter._country_analysers[0].issues[0]) == RedirectedWdItemIssue
    assert len(reporter.fallback_issues) == 1


def test_save_fallback_issues(tmpdir, invalidWdItemFormatIssue, redirectedWdItemIssue):
    prefix = "osmose_output_folder"
    reporter = OsmoseReporter(
        tmpdir,
        prefix,
        "tests/res/test_andora",
        send_report=False,
    )

    reporter.fallback_issues.append(invalidWdItemFormatIssue)
    reporter.fallback_issues.append(redirectedWdItemIssue)
    reporter.close()

    folder_list = os.listdir(tmpdir)
    assert len(folder_list) == 1, "not exactly one folder created"
    assert folder_list[0].startswith(prefix), "folder name not matching"
    file_list = os.listdir(os.path.join(tmpdir, folder_list[0]))
    assert len(file_list) == 2, "not exactly 2 file created"
    file_list = sorted(file_list)
    assert file_list[0] == f"fallback_issues.csv", "file name not matching"


def test_save_and_send(
    tmpdir, invalidWdItemFormatIssue, redirectedWdItemIssue, httpserver: HTTPServer
):
    prefix = "osmose_output_folder"
    reporter = OsmoseReporter(
        tmpdir,
        prefix,
        "tests/res/test_andora",
        api_host=f"http://{httpserver.host}:{httpserver.port}",
        send_report=True,
    )

    # reporter._country_analysers.append(_CountryAnalyser(1, "TST", "Test", None))
    reporter._country_analysers[0].issues.append(invalidWdItemFormatIssue)
    reporter._country_analysers[0].issues.append(redirectedWdItemIssue)

    httpserver.expect_ordered_request(
        "/control/send-update", method="POST"
    ).respond_with_data("OK", status=200)
    httpserver.expect_ordered_request("/dummy").respond_with_data(
        "dummy"
    )  # needed, otherwhise passes when nothing is received

    reporter.close()
    requests.get(httpserver.url_for("/dummy"))

    httpserver.check_assertions()  # check first update and then dummy received

    folder_list = os.listdir(tmpdir)
    assert len(folder_list) == 1, "not exactly one folder created"
    assert folder_list[0].startswith(prefix), "folder name not matching"
    file_list = os.listdir(os.path.join(tmpdir, folder_list[0]))
    assert len(file_list) == 1, "not exactly one file created"
    assert file_list[0] == f"report_AD_andorra.xml", "file name not matching"


def test_dont_send_report(
    tmpdir,
    invalidWdItemFormatIssue,
    redirectedWdItemIssue,
    httpserver: HTTPServer,
):
    prefix = "osmose_output_folder"
    reporter = OsmoseReporter(
        tmpdir,
        prefix,
        "tests/res/test_andora/",
        api_host=f"http://{httpserver.host}:{httpserver.port}",
        send_report=False,
    )

    # reporter._country_analysers.append(_CountryAnalyser(1, "TST", "Test", None))
    reporter._country_analysers[0].issues.append(invalidWdItemFormatIssue)
    reporter._country_analysers[0].issues.append(redirectedWdItemIssue)

    httpserver.expect_ordered_request("/dummy").respond_with_data(
        "dummy"
    )  # needed, otherwhise passes when nothing is received

    reporter.close()
    requests.get(httpserver.url_for("/dummy"))

    httpserver.check_assertions()  # check first update and then dummy received

    folder_list = os.listdir(tmpdir)
    assert len(folder_list) == 1, "not exactly one folder created"
    assert folder_list[0].startswith(prefix), "folder name not matching"
    file_list = os.listdir(os.path.join(tmpdir, folder_list[0]))
    assert len(file_list) == 1, "not exactly one file created"
    assert file_list[0] == f"report_AD_andorra.xml", "file name not matching"


def test_send_only(
    tmpdir,
    httpserver: HTTPServer,
):
    report_folder_name = "osmose_output_folder_X_X_X"
    path_2_reports = os.path.join(tmpdir, report_folder_name)
    reporter = OsmoseReporter(
        tmpdir,
        "<no-prefix>",
        "tests/res/test_andora/",
        api_host=f"http://{httpserver.host}:{httpserver.port}",
        send_report=True,
    )

    reporter._country_analysers = []  # clear list
    reporter._country_analysers.append(_CountryAnalyser(1, "TST-1", "Test1", None))
    reporter._country_analysers.append(_CountryAnalyser(12, "TST-2", "Test2", None))

    report_file1 = "report_TST-1_Test1.xml"
    report_file2 = "report_TST-2_Test2.xml"
    if not os.path.exists(path_2_reports):
        os.mkdir(path_2_reports)
    with open(os.path.join(path_2_reports, report_file1), "w") as f1:
        f1.write("something in report1")
    with open(os.path.join(path_2_reports, report_file2), "w") as f2:
        f2.write("something in report2")

    httpserver.expect_ordered_request(
        "/control/send-update", method="POST"
    ).respond_with_data("OK", status=200)
    httpserver.expect_ordered_request(
        "/control/send-update", method="POST"
    ).respond_with_data("OK", status=200)
    httpserver.expect_ordered_request("/dummy").respond_with_data(
        "dummy"
    )  # needed, otherwhise passes when nothing is received

    reporter.send_reports(path_2_reports)
    requests.get(httpserver.url_for("/dummy"))

    httpserver.check_assertions()  # check first&second update and then dummy received

    folder_list = os.listdir(tmpdir)
    assert len(folder_list) == 1, "not exactly one folder created"
    assert folder_list[0].startswith(report_folder_name), "folder name not matching"
    file_list = os.listdir(os.path.join(tmpdir, folder_list[0]))
    assert len(file_list) == 2, "not exactly one file created"
    file_list = sorted(file_list)  # needed to pass test on github ci
    assert file_list[0] == report_file1, "file name not matching"
    assert file_list[1] == report_file2, "file name not matching"
