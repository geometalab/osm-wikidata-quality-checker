from osm_wikidata_quality_checker import __version__ as v_init


def test_version_init():
    assert v_init == "1.3.0"
