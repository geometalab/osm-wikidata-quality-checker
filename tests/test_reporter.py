import csv
import json

import pytest
import xlrd

from osm_wikidata_quality_checker.checker_issues import (
    CheckerIssue,
    InvalidWdItemFormatIssue,
    RedirectedWdItemIssue,
)
from osm_wikidata_quality_checker.osm_database import OsmNode
from osm_wikidata_quality_checker.reporter import (
    CsvReporter,
    ExcelReporter,
    SummaryReporter,
)


def test_csv_reporter(
    tmpdir, invalidWdItemFormatIssue: CheckerIssue, redirectedWdItemIssue: CheckerIssue
):
    reporter = CsvReporter(tmpdir.mkdir("reports"), "csv_reporter_test.csv")
    reporter.open()
    reporter.report([invalidWdItemFormatIssue, redirectedWdItemIssue])
    reporter.flush()
    reporter.close()

    with open(reporter.path_2_report, "r", encoding="utf8") as file:
        csv_file = csv.reader(file)
        lines = [l for l in csv_file]
        assert len(lines) == 3

        issue_line = lines[1]
        assert int(issue_line[2]) == invalidWdItemFormatIssue.osm.oid
        assert int(issue_line[4]) == invalidWdItemFormatIssue.qid

        issue_line = lines[2]
        assert int(issue_line[2]) == redirectedWdItemIssue.osm.oid
        assert int(issue_line[4]) == redirectedWdItemIssue.qid


def test_excel_reporter(
    tmpdir, invalidWdItemFormatIssue: CheckerIssue, redirectedWdItemIssue: CheckerIssue
):
    filename = "test_excel_reporter.xlsx"
    reporter = ExcelReporter(tmpdir, filename)
    # reporter = ExcelReporter("./tests", filename)  # DEBUG only
    reporter.open()
    reporter.report([invalidWdItemFormatIssue, redirectedWdItemIssue])
    reporter.flush()
    reporter.close()

    # see https://stackoverflow.com/questions/64264563/attributeerror-elementtree-object-has-no-attribute-getiterator-when-trying
    xlrd.xlsx.ensure_elementtree_imported(False, None)
    xlrd.xlsx.Element_has_iter = True

    with xlrd.open_workbook(reporter.path_2_report) as wb:
        assert wb.sheet_names()[0] == "issues"
        ws = wb.sheet_by_index(0)
        assert ws.nrows == 7


def test_summary_reporter(
    tmpdir, invalidWdItemFormatIssue: CheckerIssue, redirectedWdItemIssue: CheckerIssue
):
    filename = "test_summary_reporter.json"
    reporter = SummaryReporter(tmpdir, filename)
    reporter.open()
    reporter.report([invalidWdItemFormatIssue, redirectedWdItemIssue])
    reporter.flush()
    reporter.close()

    with open(reporter.path_2_report, "r") as f:
        actual = json.load(f)

    assert actual["total"] == 2
    assert len(actual["categories"]) == 2
    assert actual["categories"][invalidWdItemFormatIssue.get_type_name()] == 1
    assert actual["categories"][redirectedWdItemIssue.get_type_name()] == 1
