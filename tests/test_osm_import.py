import logging
from datetime import datetime
from time import perf_counter

import pytest

from osm_wikidata_quality_checker.osm_database import OsmDatabase
from osm_wikidata_quality_checker.osm_import import _osm_import_mp, osm_import

# LOGGER = logging.getLogger(__name__)


test_osm_import_MAX_TIME = 15
test_osm_import_MAX_TIME_mp = 40


@pytest.mark.slow
# @pytest.mark.timeout(test_osm_import_MAX_TIME * 3)
def test_osm_import(tmpdir):
    osm_test_dump = "./tests/dumps/andorra-latest.osm.pbf"
    osm_test_db = tmpdir.join("test_osm_import.db")
    runtime_log = "./tests/dumps/osm_import_runtime.log"

    start = perf_counter()
    osm_import(
        osm_test_db,
        osm_test_dump,
        bulk_size=100,
        use_multiprocessing=False,
        use_file_idx_cache=False,
    )
    import_time_s = perf_counter() - start

    with open(runtime_log, mode="a", encoding="utf-8") as file:
        file.write(f"{datetime.now()}\t{osm_test_dump}\t{import_time_s}s\n")

    db = OsmDatabase(osm_test_db)

    assert (
        import_time_s < test_osm_import_MAX_TIME
    ), f"runtime of osm import was {import_time_s}s (should be lower than {test_osm_import_MAX_TIME}s)"
    assert db.get_node(58957648)  # Node: Andorra la Vella (58957648)
    assert db.get_area(2 * 3657693 + 1)  # Relation: Andorra la Vella (3657693)


# TODO that test work only when run alone, but not when run all. PROBLEM: very large memory usage (why is unclear)

# @pytest.mark.slow
# # @pytest.mark.timeout(test_osm_import_MAX_TIME * 3)
# def test_osm_import_mp(tmpdir):
#     osm_test_dump = "./tests/dumps/andorra-latest2.osm.pbf"
#     osm_test_db = tmpdir.join("test_osm_import_mp.db")
#     runtime_log = "./tests/dumps/osm_import_runtime_mp.log"

#     start = perf_counter()
#     osm_import(
#         osm_test_db,
#         osm_test_dump,
#         bulk_size=100,
#         use_multiprocessing=True,
#         use_file_idx_cache=False,
#     )
#     import_time_s = perf_counter() - start

#     with open(runtime_log, mode="a", encoding="utf-8") as file:
#         file.write(f"{datetime.now()}\t{osm_test_dump}\t{import_time_s}s\n")

#     db = OsmDatabase(osm_test_db)

#     assert (
#         import_time_s < test_osm_import_MAX_TIME_mp
#     ), f"runtime of osm import was {import_time_s}s (should be lower than {test_osm_import_MAX_TIME}s)"
#     assert db.get_node(58957648)  # Node: Andorra la Vella (58957648)
#     assert db.get_area(2 * 3657693 + 1)  # Relation: Andorra la Vella (3657693)


# @pytest.mark.no_gitlab_ci
# @pytest.mark.slow
# # @pytest.mark.timeout(test_osm_import_MAX_TIME * 3)
# def test_osm_import_mp(tmpdir):
#     osm_test_dump = "./tests/dumps/andorra-latest.osm.pbf"
#     osm_test_db = tmpdir.join("test_osm_import_mp.db")

#     _osm_import_mp(
#         osm_test_db,
#         osm_test_dump,
#         bulk_size=100,
#         use_file_idx_cache=True,
#         join_sleep_time=1,
#     )

#     db = OsmDatabase(osm_test_db)

#     assert db.get_node(58957648)  # Node: Andorra la Vella (58957648)
#     assert db.get_area(2 * 3657693 + 1)  # Relation: Andorra la Vella (3657693)


# @pytest.mark.no_gitlab_ci
# @pytest.mark.slow
# @pytest.mark.timeout(test_osm_import_MAX_TIME * 3)
def test_osm_import_mp_fail(tmpdir):
    osm_test_dump = "./tests/dumps/andorra-latest_corrupt.osm.pbf"
    osm_test_db = tmpdir.join("test_osm_import_mp_fail.db")

    with pytest.raises(Exception) as excinfo:
        _osm_import_mp(
            osm_test_db,
            osm_test_dump,
            bulk_size=100,
            use_file_idx_cache=False,
            join_sleep_time=1,
        )
    assert (
        "osm import process 'osm import _OsmDumpPbfReaderNode' failed with exitcode 1"
        in str(excinfo.value)
    )
