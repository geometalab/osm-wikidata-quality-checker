import os
from importlib import reload
from pathlib import Path

import dotenv

import osm_wikidata_quality_checker.main
from osm_wikidata_quality_checker import env
from osm_wikidata_quality_checker.main import run


def test_run():
    # Override with test env
    dotenv.load_dotenv("tests/test.env", override=True)
    # Reload env so the values from test env is loaded
    reload(env)
    reload(osm_wikidata_quality_checker.main)
    # Make sure the test env is loaded correctly
    assert env.INSTANCE_NAME == "pytest test"
    # Run the main function (the actual test))
    # TODO currently, the test does not do much (download,import,check are all no). Timeout issues. Retry when mp can be disabled
    assert osm_wikidata_quality_checker.main.run() == None
