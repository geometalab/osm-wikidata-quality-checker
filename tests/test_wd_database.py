import pytest

from osm_wikidata_quality_checker.wd_database import WdCoordinate, WdDatabase, WdItem


@pytest.fixture
def database():
    return WdDatabase(":memory:")


# CONSIDER TE-JE ist das noch nötig?
@pytest.mark.filterwarnings("ignore::RuntimeWarning")
def test_primary_key_ignore(database: WdDatabase):
    item1 = WdItem()
    item1.qid = 1
    item2 = WdItem()
    item2.qid = 1

    database.add_all([item1, item2])

    item3 = WdItem()
    item3.qid = 1

    database.add_all([item3])


def test_add_and_get_wditem(database: WdDatabase):
    item1 = WdItem()
    item1.qid = 100

    coord1 = WdCoordinate(11.111, 22.222)
    coord2 = WdCoordinate(33.333, 44.444)

    item1.coordinates.append(coord1)
    item1.coordinates.append(coord2)

    database.add_all([item1])

    db_item = database.get(100)

    assert db_item
    assert db_item.qid == 100
    assert len(db_item.coordinates) == 2


def test_add_all_wditem(database: WdDatabase):
    item1 = WdItem()
    item1.qid = 100

    item2 = WdItem()
    item2.qid = 200

    database.add_all([item1, item2])

    actual1 = database.get(100)
    actual2 = database.get(200)

    assert actual1
    assert actual1.qid == 100
    assert actual2
    assert actual2.qid == 200
