from random import randint, random

import pytest
import requests

from osm_wikidata_quality_checker.checker_issues import (
    ChangeFix,
    DeleteFix,
    NodeLink,
    RedirectedWdItemIssue,
)
from osm_wikidata_quality_checker.osm_database import (
    OsmArea,
    OsmNode,
    OsmObject,
    OsmRelation,
    OsmWay,
)
from osm_wikidata_quality_checker.osmose_reporter import InvalidWdItemFormatIssue
from osm_wikidata_quality_checker.wd_database import WdCoordinate, WdItem

# common fixtures


@pytest.fixture(scope="function")
def random_id():
    return randint(1, 10000)


@pytest.fixture(scope="function")
def random_coord():
    return random() * 90


def init_osm_object(osm: OsmObject):
    osm.oid = random_id()
    osm.tags = {"wikidata": "Q" + random_id()}
    osm.user = "OWQC"
    osm.version = 1


@pytest.fixture(scope="function")
def has_internet():
    url = "http://www.google.com"
    timeout = 5
    try:
        requests.get(url, timeout=timeout)
        return True
    except (requests.ConnectionError, requests.Timeout):
        return False


@pytest.fixture(scope="function")
def osmNode():
    osm = OsmNode()
    osm.oid = 132591
    osm.tags = {
        "name": "Woolverstone",
        "place": "village",
        "wikidata": "Q2117457",
        "wikipedia": "en:Wolverstone",
    }
    osm.user = "Edward"
    osm.version = 3
    # specific
    osm._lat = 52.0027574
    osm._lon = 1.1820034
    return osm


@pytest.fixture(scope="function")
def osmWay():
    osm = OsmWay()
    osm.oid = 1216
    osm.tags = {
        "highway": "trunk",
        "int_ref": "E 15",
        "junction": "roundabout",
        "lanes": "3",
        "lit": "yes",
        "maxspeed": "60 mph",
        "maxspeed:type": "GB:national",
        "name": "Bignell's Corner",
        "national_highways:area": "DBFO5",
        "oneway": "yes",
        "operator": "National Highways",
        "operator:wikidata": "Q5760006",
        "operator:wikipedia": "en:National Highways",
        "surface": "asphalt",
    }
    osm.user = "Thomas Jarvis"
    osm.version = 24
    # specific
    osm.is_closed = 0
    osm._wkb_hex = "01020000000200000070928BD6F61BCDBFE0F7CA170FD8494065A88AA9F413CDBFFBEF1C250AD84940"
    return osm


@pytest.fixture(scope="function")
def osmArea():
    osm = OsmArea()
    osm.oid = 1282
    osm.tags = {
        "amenity": "school",
        "building": "school",
        "isced:level": "2",
        "name": "Heron Hall Academy",
        "old_name": "Enfield Technical College and Middlesex Polytechnic",
        "wikidata": "Q66229995",
    }
    osm.user = "Edward"
    osm.version = 7
    # specific
    osm.uid = 2565
    osm.is_way = 0
    osm.is_multipolygon = 0
    osm._wkb_hex = "0106000000010000000103000000030000000B000000B0123D3ABF72AABF52F01472A5D24940D52DF136250FAABF54E81780A1D24940D4E81F8EBFFEA9BF55AF1177ABD249404C01C4B876FDA9BF720A3C41ACD2494006BEA25BAFE9A9BF664B5645B8D24940CC04680014C8A9BF5B75C2F0B6D24940E536D03241C3A9BF1E7FB3DEB9D249408014D09F9163AABF44149337C0D249407B71981E0368AABF9FAD8383BDD249404C851D6BEB4CAABF82F63471BCD24940B0123D3ABF72AABF52F01472A5D24940050000003F5AF7EA3E5BAABF0305DEC9A7D24940987D68305750AABFADA6EB89AED2494044E3E4D94015AABFB4CBB73EACD24940EBBF73942820AABF766AD37DA5D249403F5AF7EA3E5BAABF0305DEC9A7D2494005000000B435C75E9445AABF21DF4A1BB3D24940A09B470F8D38AABF1718682DBBD249406AFEF38F1903AABF7EA19119B9D249407E9873DF2010AABF88687407B1D24940B435C75E9445AABF21DF4A1BB3D24940"
    osm.num_rings_outer = 1
    osm.num_rings_outer = 2
    return osm


@pytest.fixture(scope="function")
def osmRelation():
    osm = OsmRelation()
    osm.oid = 58
    osm.tags = {
        "alt_name": "The Lake",
        "intermittent": "no",
        "layer": "1",
        "name": "Verulamium Lake",
        "natural": "water",
        "salt": "no",
        "tidal": "no",
        "type": "multipolygon",
        "water": "lake",
        "wikidata": "Q24673970",
    }
    osm.user = "samthecrazyman"
    # specific
    osm.version = 8
    osm.members = [
        [3649462, "outer", "w"],
        [8128679, "inner", "w"],
        [8128680, "inner", "w"],
    ]
    return osm


@pytest.fixture(scope="function")
def nodeLink(osmNode: OsmNode, wdItem: WdItem):
    return NodeLink(osmNode, wdItem, "wikidata")


@pytest.fixture(scope="function")
def invalidWdItemFormatIssue(osmNode):
    return InvalidWdItemFormatIssue(
        osmNode, "wikidata", "XXX", fix=DeleteFix("wikidata")
    )


@pytest.fixture(scope="function")
def redirectedWdItemIssue(osmNode):
    return RedirectedWdItemIssue(
        osmNode, "wikidata", 1, 2, fix=ChangeFix("wikidata", "Q2")
    )


@pytest.fixture(scope="function")
def wdItem():
    wd = WdItem()
    wd.qid = 2400788
    wd.names = {
        "fr": "universit\u00e9 du troisi\u00e8me \u00e2ge",
        "en": "University of the Third Age",
        "eo": "Universitato de la tria a\u011do",
        "ja": "University of the Third Age",
        "pl": "Uniwersytet Trzeciego Wieku",
        "cs": "Univerzita t\u0159et\u00edho v\u011bku",
        "be-tarask": "\u0423\u043d\u0456\u0432\u044d\u0440\u0441\u044b\u0442\u044d\u0442 \u0442\u0440\u044d\u0446\u044f\u0433\u0430 \u0432\u0435\u043a\u0443",
        "en-ca": "University of the Third Age",
        "en-gb": "University of the Third Age",
        "nl": "University of the Third Age",
        "es": "Universidad de Tiempo Libre",
        "ru": "\u0423\u043d\u0438\u0432\u0435\u0440\u0441\u0438\u0442\u0435\u0442 \u0442\u0440\u0435\u0442\u044c\u0435\u0433\u043e \u0432\u043e\u0437\u0440\u0430\u0441\u0442\u0430",
        "pt": "Universidade da Terceira Idade",
        "ar": "\u062c\u0627\u0645\u0639\u0629 \u0627\u0644\u0639\u0645\u0631 \u0627\u0644\u062b\u0627\u0644\u062b",
        "sq": "Universiteti i Mosh\u00ebs s\u00eb Tret\u00eb",
        "tr": "\u00dc\u00e7\u00fcnc\u00fc Nesil \u00dcniversite",
        "it": "universit\u00e0 della terza et\u00e0",
        "sk": "Univerzita tretieho veku",
    }
    wd.country_qid = 145
    wd.replaced_by_qid = None
    wd.instances_of = set([708676])
    wd.subclasses_of = set([3914])
    wd.street_addresses = [
        {
            "text": "Unit 104, The Foundry Business Centre, 156 Blackfriars Road, London, England, SE1 8EN",
            "language": "en-gb",
        }
    ]
    wd.postal_codes = ["SE1 8EN"]
    wd.located_on_streets = [[15030879, []]]
    cord = WdCoordinate(
        50.641111111111, 4.6680555555556
    )  # NOTE not real coordinate of this item
    wd.coordinates = [cord]
    wd.all_instances_and_subclasses = set()

    return wd
