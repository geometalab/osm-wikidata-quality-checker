from osm_wikidata_quality_checker.env import (
    PATH_2_OSM_DB,
    PATH_2_OSM_DUMP,
    PATH_2_WD_DB,
    PATH_2_WD_DUMP,
)


def test_env_path():
    assert PATH_2_OSM_DB
    assert PATH_2_OSM_DUMP
    assert PATH_2_WD_DB
    assert PATH_2_WD_DUMP
