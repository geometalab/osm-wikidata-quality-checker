from osm_wikidata_quality_checker.checker import (
    _does_wd_item_exist,
    _is_redirected,
    _is_valid_format,
    check,
)
from osm_wikidata_quality_checker.checker_issues import (
    InvalidWdItemFormatIssue,
    NotExistingWdItemIssue,
    RedirectedWdItemIssue,
)
from osm_wikidata_quality_checker.osm_database import OsmNode
from osm_wikidata_quality_checker.wd_redirects_database import (
    WdRedirect,
    WdRedirectsDatabase,
)


def test_is_valid_format():
    wikidata_key = "wikidata"
    # test positive
    assert _is_valid_format("Q123", wikidata_key)
    assert _is_valid_format("Q32123", wikidata_key)

    # test negative
    assert _is_valid_format("Q", wikidata_key) == False
    assert _is_valid_format("123", wikidata_key) == False
    assert _is_valid_format("123Q", wikidata_key) == False
    assert _is_valid_format("13Q123", wikidata_key) == False
    assert _is_valid_format("q123", wikidata_key) == False

    # test ignore lexems
    assert _is_valid_format("L1234", "etymology:" + wikidata_key) == None
    assert _is_valid_format("L1234-F123", "name:etymology:" + wikidata_key) == None
    assert (
        _is_valid_format("L1234-S123", "local_name:etymology:" + wikidata_key) == None
    )
    assert _is_valid_format("L1234", wikidata_key) == False
    assert _is_valid_format("L1234-X1", wikidata_key) == False
    assert _is_valid_format("Lara-X123", wikidata_key) == False

    # test ignore keys ending with ':missing'
    assert _is_valid_format("Peter", wikidata_key + ":missing") == None
    assert _is_valid_format("Peter", "missing:" + wikidata_key) == False


def test_is_valid_format_limiter(osmNode: OsmNode):
    class db_fake:
        @staticmethod
        def get(qid: int):
            return None

    # positives
    osmNode.tags["wikidata"] = "Q11111111111111111111111"
    assert isinstance(check(osmNode, db_fake, db_fake), NotExistingWdItemIssue)
    osmNode.tags["wikidata"] = "Q11111111111111111111111;Q11111111111111111111111"
    assert isinstance(check(osmNode, db_fake, db_fake), NotExistingWdItemIssue)

    # negative
    osmNode.tags["wikidata"] = ";;Q2117457;;"
    assert isinstance(check(osmNode, db_fake, db_fake), InvalidWdItemFormatIssue)
    osmNode.tags["wikidata"] = "Q123|Q124"
    osmNode.tags["wikidata:missing"] = "Q1"
    assert isinstance(check(osmNode, db_fake, db_fake), InvalidWdItemFormatIssue)


def test_is_check_redirected(osmNode: OsmNode):
    class db_fake:
        @staticmethod
        def get(qid: int):
            return None

    wd_redirects_db = WdRedirectsDatabase(":memory:")
    wd_redirects_db.add_all([WdRedirect(1, 2)])

    osmNode.tags["wikidata"] = "Q1"
    assert isinstance(check(osmNode, db_fake, wd_redirects_db), RedirectedWdItemIssue)


def test_is_check_exist_but_not_in_index(osmNode: OsmNode):
    class db_fake:
        @staticmethod
        def get(qid: int):
            return None

    osmNode.tags["wikidata"] = "Q1"
    assert check(osmNode, db_fake, db_fake) is None


def test_is_redirected():
    wd_redirects_db = WdRedirectsDatabase(":memory:")
    redirect = WdRedirect(1, 2)

    redirect_2 = WdRedirect(99, 1)

    redirect_loop_1 = WdRedirect(3, 4)
    redirect_loop_2 = WdRedirect(4, 5)
    redirect_loop_3 = WdRedirect(5, 3)

    wd_redirects_db.add_all(
        [redirect, redirect_2, redirect_loop_1, redirect_loop_2, redirect_loop_3]
    )

    assert _is_redirected(wd_redirects_db, 1) == 2
    assert _is_redirected(wd_redirects_db, 99) == 2
    assert _is_redirected(wd_redirects_db, 3) is None
    assert _is_redirected(wd_redirects_db, 42) is None
    assert _is_redirected(wd_redirects_db, None) is None


def test_does_item_exist():
    assert _does_wd_item_exist("Q1")
    assert not _does_wd_item_exist("Q-1")
