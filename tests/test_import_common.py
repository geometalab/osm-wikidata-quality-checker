from multiprocessing import JoinableQueue
from queue import Empty

import pytest

from osm_wikidata_quality_checker.import_common import InsertBuffer
from osm_wikidata_quality_checker.osm_database import OsmDatabase, OsmNode


def test_insertbuffer_with_queue():
    queue = JoinableQueue()
    handler = InsertBuffer(queue.put, 10)
    test_list = []
    for i in range(0, 25):
        obj = OsmNode()
        obj.oid = i
        test_list.append(obj)
        handler.insert(obj)

    queue_part = queue.get()
    test_part = test_list[:10]
    assert len(queue_part) == len(test_part)
    assert all(a.oid == b.oid for a, b in zip(queue_part, test_part))

    queue_part = queue.get()
    test_part = test_list[10:20]
    assert len(queue_part) == len(test_part)
    assert all(a.oid == b.oid for a, b in zip(queue_part, test_part))

    with pytest.raises(Empty):
        queue.get_nowait()

    handler.flush()
    queue_part = queue.get()
    test_part = test_list[20:]
    assert len(queue_part) == len(test_part)
    assert all(a.oid == b.oid for a, b in zip(queue_part, test_part))


def test_insertbuffer_with_db():
    path_2_db = ":memory:"
    db = OsmDatabase(path_2_db)
    buffer = InsertBuffer(db.add_all, 10)
    test_list = []
    for i in range(0, 25):
        obj = OsmNode()
        obj.oid = i
        test_list.append(obj)
        buffer.insert(obj)
    buffer.flush()

    list_in_db = [item for sub_list in db.get_all(100) for item in sub_list]

    assert all(
        a.oid == b
        for a, b in zip(
            list_in_db,
            range(0, 25),
        )
    )


# HINT import_controller is tested within test_x_import modules
