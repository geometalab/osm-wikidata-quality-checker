import pytest
from pytest import fixture

from osm_wikidata_quality_checker.checker_issues import (
    AreaLink,
    NodeLink,
    RelationLink,
    WayLink,
    create_link,
)
from osm_wikidata_quality_checker.osm_database import (
    OsmArea,
    OsmNode,
    OsmRelation,
    OsmWay,
)
from osm_wikidata_quality_checker.wd_database import WdItem


@fixture
def wd_item():
    item = WdItem()
    item.qid = 1
    return item


def test_create_link_node(wd_item):
    node = OsmNode()
    node.oid = 1

    assert isinstance(create_link(node, wd_item, "wikidata"), NodeLink)


def test_create_link_way(wd_item):
    way = OsmWay()
    way.oid = 1

    assert isinstance(create_link(way, wd_item, "wikidata"), WayLink)


def test_create_link_rel(wd_item):
    rel = OsmRelation()
    rel.oid = 1

    assert isinstance(create_link(rel, wd_item, "wikidata"), RelationLink)


def test_create_link_area(wd_item):
    area = OsmArea()
    area.oid = 1

    assert isinstance(create_link(area, wd_item, "wikidata"), AreaLink)


def test_create_link_error(wd_item):
    with pytest.raises(TypeError):
        create_link(object(), wd_item, "wikidata")
