import warnings

import pytest

from osm_wikidata_quality_checker.check_osmose_country_commit_hash import (
    _get_actual_commit_hash,
    _load_last_commit_hash,
    check_commit_hash,
)


def test_get_actual_hash():
    hash = _get_actual_commit_hash()
    assert type(hash) is str
    assert len(hash) == 40


def test_load_last_hash():
    hash = _load_last_commit_hash(
        "tests/res/test_andora/osmose_country_config_commit_hash.txt"
    )
    assert type(hash) is str
    assert len(hash) == 40


def test_hash(tmp_path):
    tmp_path = tmp_path / "osmose_country_config_commit_hash.txt"
    tmp_path.write_text(_get_actual_commit_hash())
    assert not check_commit_hash(
        "tests/res/test_andora/osmose_country_config_commit_hash.txt"
    )
    assert check_commit_hash(tmp_path)
    # assert not check_commit_hash(commit_hash_fail)
